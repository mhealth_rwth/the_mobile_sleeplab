package de.msl.csvexporter;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class StartUp extends AppCompatActivity {

    long recordid;
    ArrayList<String> allPlugins;

    private ExportHelper helper;
    private EditText editTextName;
    private TextView infoText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up);

        infoText = (TextView) findViewById(R.id.infoText);

        IntentFilter intentFilter = new IntentFilter(ExportHelper.finishedTaskIntent);
        registerReceiver(br, intentFilter);

        checkPermission();

        Intent intent = getIntent();
        recordid = intent.getLongExtra("recordid", 0);
        allPlugins = intent.getStringArrayListExtra("allPlugins");

        editTextName = (EditText) findViewById(R.id.editTextFilename);
        editTextName.setText(String.valueOf(recordid));

        // Use ExportHelper class to receive data
        helper = new ExportHelper(this, allPlugins, intent, recordid);
    }

    public void checkPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 457);

        }
    }

    @Override
    public void onStop(){
        unregisterReceiver(br);
        super.onStop();
    }

    public void exportCsv(View view){
        infoText.setText("Export Ongoing");
        helper.start(editTextName.getText().toString());
    }

    BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(ExportHelper.finishedTaskIntent)){
                infoText.setText("Export Completed");
            }
        }
    };

}
