package de.msl.csvexporter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


/**
 * Created by Andreas on 10.04.2016.
 */
public class ExportHelper {
    private Context context;

    protected static final String finishedTaskIntent = "de.msl.csvexporter.finished";

    int ct = 0;

    long recordid;
    ArrayList<String> allPlugins;
    Map<String, ArrayList<String>> exportList = new TreeMap<>();
    List<String> dataFormat = new ArrayList<>();
    Intent oldIntent;
    String resultString = "time_from;";
    public static final String PATH = "/sdcard/msl/msl_";

    private String name;


    public ExportHelper(Context context, ArrayList<String> allPlugins, Intent oldIntent, long recordid){
        this.context = context;
        this.allPlugins = allPlugins;
        this.oldIntent = oldIntent;
        this.recordid = recordid;
    }


    public void start(String name){
        this.name = name;
        // Start Async Task to allow background processing
        new DataCalculator().execute();
    }

    /**
     * Iterate over all plugins and fetch data
     */
    private void start2(){
        if (allPlugins != null && allPlugins.size() > 0){

            int ctr = 0;

            for(String plugin : allPlugins){
                ArrayList<String> allDataFields = oldIntent.getStringArrayListExtra(plugin);
                for(String s : allDataFields){
                    if((!s.equals("time_from"))&&(!s.equals("_id"))&&(!s.equals("record")) &&(!s.equals("time_to"))){
                        dataFormat.add(ctr, s);
                        ctr++;
                    }
                }
            }

            for (String plugin : allPlugins) {
                ArrayList<String> allDataFields = oldIntent.getStringArrayListExtra(plugin);
                getData(plugin, allDataFields);
            }
        }

        resultString += "\n";
        printList();

        context.sendBroadcast(new Intent(finishedTaskIntent));

        }

    private class DataCalculator extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... params) {
            start2();
            return null;
        }

    }

    /**
     * Fetches data from the content provider
     * @param plugin plugin, to receive data for
     * @param allDataFields data fields of respective plugin
     * @return
     */
    public String getData(String plugin, ArrayList<String> allDataFields){

        try {
            Uri uri = Uri.parse("content://de.msl.cpmobilesleeplab/" + plugin + "/" + recordid);

            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();

            int timeFromPos = allDataFields.indexOf("time_from");
            int recordPos = allDataFields.indexOf("record");
            int idPos = allDataFields.indexOf("_id");
            int timeToPos = allDataFields.indexOf("time_to");

            for (String s : allDataFields) {
                if ((!s.equals("time_from")) && (!s.equals("_id")) && (!s.equals("record")) && (!s.equals("time_to"))) {
                    resultString += s;
                    resultString += ";";
                }
            }

            while (!cursor.isAfterLast()) {
                boolean newV = false;
                String timeFrom = cursor.getString(timeFromPos);
                ArrayList<String> currentEntry;
                currentEntry = exportList.get(timeFrom);
                if (currentEntry == null) {
                    newV = true;
                    currentEntry = new ArrayList<>();
                    for (int i = 0; i < dataFormat.size(); i++) {
                        currentEntry.add(i, "");
                    }
                }

                for (String s : allDataFields) {
                    int pos = allDataFields.indexOf(s);
                    if ((pos != timeFromPos) && (pos != idPos) && (pos != recordPos) && (pos != timeToPos)) {
                        currentEntry.set(dataFormat.indexOf(s), cursor.getString(pos));
                    }
                }

                if (newV) {
                    exportList.put(timeFrom, currentEntry);
                }

                ct++;
                cursor.moveToNext();
            }
            cursor.close();
            return resultString;
        }
        catch (Exception e){
            return  "";
        }
    }

    /**
     * Generates the csv file
     */
    void  printList(){

        File file = null;
        FileOutputStream fileOutputStream;

        String result = "";

        try {
            file = new File(PATH+ name + ".csv");
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(resultString.getBytes());

            for (TreeMap.Entry<String, ArrayList<String>> vals : exportList.entrySet()) {
                result = vals.getKey();
                result += ";";
                for (String s : vals.getValue()) {
                    result += s;
                    result += ";";
                }
                result += "\n";
                fileOutputStream.write(result.getBytes());
            }
            fileOutputStream.close();
        }
        catch(Exception ex){

        }
    }
}
