package de.msl.soundplugin;

import android.app.Notification;
import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.io.File;
import java.io.IOException;


public class GenerateService extends Service {

    private Runnable runnable = null;
    private static final String TAG = "GENERATE_SERVICE";
    protected static final String ACTION_START_FOREGROUND = "de.msl.soundplugin.startinforeground";

    protected Thread generatorThread = null;
    protected Handler generateHandler = null;

    private MediaRecorder mediaRecorder = null;
    private boolean firstValueInserted = false;

    //String path = "/sdcard/msl/msl_";
    String path = Environment.getExternalStorageDirectory() + "/msl/msl_";

    String name = "";

    private int nrOfChannels = 1;


    @Override
    public void onCreate(){
        super.onCreate();
    }

    @Override
    public void onDestroy(){

        generateHandler.removeCallbacksAndMessages(null);
        runnable = null;
        generateHandler = null;
        generatorThread = null;
        stop();
        super.onDestroy();

    }

    public GenerateService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)  {
        firstValueInserted = false;
        // set if mono or stereo
        if(intent.getBooleanExtra("mono", true)){
            nrOfChannels = 1;
        }
        else{
            nrOfChannels = 2;
        }

        try {
            start();
        }
        catch (Exception e){

        }
        startGenerating();
        if(intent.getAction().equals(ACTION_START_FOREGROUND)){
            startForeground(startId, new Notification());
        }

        return Service.START_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Adds the current sound level 4 times a second
     */
    private void startGenerating(){
        generateHandler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                insertRecord((int)getAmplitude(), name);
                generateHandler.postDelayed(this, 250);
            }
        };

        generateHandler.postDelayed(runnable, 250);
    }

    /**
     * Stores current sound level and source file location
     * @param value1 sound level
     * @param value2 source file
     */
    private void insertRecord(int value1, String value2) {

        if(!firstValueInserted){
            firstValueInserted = true;
            Intent intent = new Intent();
            intent.setAction("de.msl.startFinishedSoundPlugin");
            sendBroadcast(intent);
        }

        ContentResolver cr = getContentResolver();

        Long timeLong = System.currentTimeMillis();
        String time = timeLong.toString();
        ContentValues values = new ContentValues();

        values.put(SleepSQLiteHelper.COLUMN_TIME_FROM, time);
        values.put(SleepSQLiteHelper.COLUMN_TIME_TO, time);
        values.put(SleepSQLiteHelper.COLUMN_VALUE1, value1);
        values.put(SleepSQLiteHelper.COLUMN_VALUE2, value2);
        cr.insert(SleepContentProvider.CONTENT_URI, values);

    }

    public void start() throws IOException{
        // prepare Audio Record
        if (mediaRecorder == null) {
            mediaRecorder = null;
            mediaRecorder = new MediaRecorder();


            File parentDir = new File(path);
            parentDir.mkdirs();
            name = path + System.currentTimeMillis() + ".mp3";

            Log.d("Sound", name);

            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            if(nrOfChannels>1) {
               mediaRecorder.setAudioChannels(2);
            }
            else{
                mediaRecorder.setAudioChannels(1);
            }

            mediaRecorder.setAudioSamplingRate(44100);
            mediaRecorder.setAudioEncodingBitRate(96000);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mediaRecorder.setOutputFile(name);

            mediaRecorder.prepare();
            mediaRecorder.start();

        }
    }

    public void stop() {

        if (mediaRecorder != null) {
            try{
                mediaRecorder.stop();
            }
            catch(Exception e){

            }
            finally {
                mediaRecorder.reset();
                mediaRecorder.release();
                mediaRecorder = null;
            }

        }
    }

    public double getAmplitude() {
        if (mediaRecorder != null) {
            return mediaRecorder.getMaxAmplitude();
        }
        else
            return 0;
    }
}
