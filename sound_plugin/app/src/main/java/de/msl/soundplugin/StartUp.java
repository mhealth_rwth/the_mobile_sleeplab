package de.msl.soundplugin;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class StartUp extends Activity {

    private TextView textView;
    private Intent intent;

    private RadioGroup radioGroup;
    private Button buttonStop;

    private ImageView imageView;

    private boolean standAlone = true;

    private boolean mono = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        checkPermission();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up);
        textView = (TextView)findViewById(R.id.textView);
        buttonStop = (Button) findViewById(R.id.button2);
        imageView = (ImageView) findViewById(R.id.setupPic);

        imageView.setVisibility(View.INVISIBLE);

        boolean hideStop = getIntent().getBooleanExtra("hideStop", false);

        if(hideStop){
            buttonStop.setVisibility(View.INVISIBLE);
            standAlone = false;
        }


        // Radio Buttons for the decision which record source to use
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.radioButtonMono){
                    mono = true;
                    imageView.setVisibility(View.INVISIBLE);
                }
                if(checkedId == R.id.radioButtonStereo){
                    mono = false;
                    imageView.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    public void pressedStartButton(View view){
        Intent intentOutgoing = new Intent(GenerateService.ACTION_START_FOREGROUND);
        intentOutgoing.putExtra("mono", mono);
        intentOutgoing.setClass(this, GenerateService.class);
        startService(intentOutgoing);
        if(!standAlone)
            StartUp.this.finish();
    }

    public void pressedStopButton(View view){
        Intent stopIntent = new Intent();
        stopIntent.setClass(this, GenerateService.class);
        stopService(stopIntent);
    }

    /**
     * Checks if all permissions are given, else requests for them
     */
    public void checkPermission(){
        if((ContextCompat.checkSelfPermission(this,Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) || (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, 456);
            }

        }
    }
}