package de.msl.soundplugin;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by andi on 06.02.16.
 */
public class SleepSQLiteHelper extends SQLiteOpenHelper{

    public static final String TABLE_SOUNDPLUGINDATA = "soundplugindata";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_VALUE1 = "MONITOR_D013016$level_db_D000068997";
    public static final String COLUMN_VALUE2 = "MONITOR_D013016$sourcefile_db_D000068997";
    public static final String COLUMN_TIME_FROM = "time_from";
    public static final String COLUMN_TIME_TO = "time_to";
    private static final String DATABASE_NAME = "soundplugindata.db";
    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_CREATE =
            "create table " + TABLE_SOUNDPLUGINDATA + "(" + COLUMN_ID + " integer primary key autoincrement, " + COLUMN_TIME_FROM + " string not null, " + COLUMN_TIME_TO + " string not null, " + COLUMN_VALUE1 + " integer not null, " + COLUMN_VALUE2 + " string not null);";

    public SleepSQLiteHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION );
    }

    @Override
    public void onCreate(SQLiteDatabase database){
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SOUNDPLUGINDATA);
        onCreate(db);
    }
}
