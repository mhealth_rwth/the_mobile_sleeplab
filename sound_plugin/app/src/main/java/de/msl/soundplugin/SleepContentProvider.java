package de.msl.soundplugin;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

public class SleepContentProvider extends ContentProvider {

    private SleepSQLiteHelper database;

    private static final int VALUE = 1;
    private static final int VALUE_ID = 2;


    private static final String AUTHORITY = "de.msl.cpsoundplugin";
    private static final String BASE_PATH = SleepSQLiteHelper.TABLE_SOUNDPLUGINDATA;

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher
                .addURI(AUTHORITY, SleepSQLiteHelper.TABLE_SOUNDPLUGINDATA, VALUE);
        uriMatcher.addURI(AUTHORITY, SleepSQLiteHelper.TABLE_SOUNDPLUGINDATA
                + "/#", VALUE_ID);
    }

    @Override
    public boolean onCreate() {
        database = new SleepSQLiteHelper(getContext());
        return true;
    }

    // INSERT DATA

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = database.getWritableDatabase();
        long rowId = db.insert(SleepSQLiteHelper.TABLE_SOUNDPLUGINDATA, "", values);
        if(rowId > -1){
            Uri result = ContentUris.withAppendedId(CONTENT_URI, rowId);
            getContext().getContentResolver().notifyChange(result,null);
            return  result;
        }
        db.close();
        return null;
    }

    // QUERY DATA

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        SQLiteDatabase db = database.getWritableDatabase();
        SQLiteQueryBuilder sqlBuilder = new SQLiteQueryBuilder();
        sqlBuilder.setTables(SleepSQLiteHelper.TABLE_SOUNDPLUGINDATA);

        if(uriMatcher.match(uri) == VALUE_ID){
            sqlBuilder.appendWhere(SleepSQLiteHelper.COLUMN_ID + " = " + uri.getPathSegments().get(1));
        }
        if(sortOrder == null || "".equals(sortOrder)){
            sortOrder = SleepSQLiteHelper.COLUMN_ID;
        }
        Cursor c = sqlBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);

        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;

    }

    // UPDATE DATA IS NOT NEEDED FOR OUR PROJECT

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    // DELETE DATA IS NOT NEEDED FOR OUT PROJECT

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
        switch(uriMatcher.match(uri)){
            case VALUE:
                return "vnd.android.cursor.dir/vnd." + AUTHORITY + "/" + SleepSQLiteHelper.TABLE_SOUNDPLUGINDATA;
            case VALUE_ID:
                return "vnd.android.cursor.dir/vnd." + AUTHORITY + "/" + SleepSQLiteHelper.TABLE_SOUNDPLUGINDATA;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }
}