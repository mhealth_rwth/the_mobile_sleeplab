package de.msl.soundplugin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by andi on 05.02.16.
 */
public class StartReceiver extends BroadcastReceiver {

    private static final String TAG = "SOUNDPLUGIN RECV";

    @Override
    public void onReceive(Context context, Intent intentIncoming){

        Intent intentOutgoing = new Intent();

        String action = intentIncoming.getAction();

        switch (action){
            case "de.msl.startsoundplugin":
                Log.d(TAG, "started");
                intentOutgoing.setClass(context.getApplicationContext(), StartUp.class);
                intentOutgoing.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentOutgoing.putExtra("hideStop", true);
                context.getApplicationContext().startActivity(intentOutgoing);
                break;
            case "de.msl.stopsoundplugin":
                Log.d(TAG, "stopped");
                intentOutgoing.setClass(context.getApplicationContext(), GenerateService.class);
                context.stopService(intentOutgoing);
                break;
            default:
                Log.d(TAG, "unknown command");
        }

    }
}
