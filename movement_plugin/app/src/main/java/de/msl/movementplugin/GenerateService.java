package de.msl.movementplugin;

import android.app.Notification;
import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by Andreas on 13.04.2016.
 */
public class GenerateService extends Service implements SensorEventListener {

    private boolean firstValueStored = false;

    private static final String TAG = "Movement Gen";
    protected static final String ACTION_START_FOREGROUND = "de.msl.movementplugin.startinforeground";

    private SensorManager sensorManager;
    private Sensor movementSensor;

    private int accelerometerMode;

    private float offsetX;
    private float offsetY;
    private float offsetZ;

    private boolean accuracyHigh = false;

    @Override
    public void onCreate(){
        super.onCreate();

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        movementSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        boolean registerMovementSensor = sensorManager.registerListener(this, movementSensor, accelerometerMode);
        Log.d(TAG, "Movement Sensor registered: " + (registerMovementSensor ? "yes" : "no"));
    }

    @Override
    public void onDestroy(){
        sensorManager.unregisterListener(this);
        super.onDestroy();
    }

    public GenerateService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        firstValueStored = false;

        offsetX = intent.getFloatExtra("offsetX", 0.0f);
        offsetY = intent.getFloatExtra("offsetY", 0.0f);
        offsetZ = intent.getFloatExtra("offsetZ", 0.0f);

        /**
         * Set sensor delay
         */
        String accMode = intent.getStringExtra("accelerometerMode");
        switch(accMode){
            case "SENSOR_DELAY_FASTEST":
                accelerometerMode = SensorManager.SENSOR_DELAY_FASTEST;
                break;
            case "SENSOR_DELAY_NORMAL":
                accelerometerMode = SensorManager.SENSOR_DELAY_NORMAL;
                break;
            case "SENSOR_DELAY_GAME":
                accelerometerMode = SensorManager.SENSOR_DELAY_GAME;
                break;
            case "SENSOR_DELAY_UI":
                accelerometerMode = SensorManager.SENSOR_DELAY_UI;
                break;
            default:
                accelerometerMode = SensorManager.SENSOR_DELAY_NORMAL;
                break;
        }
        if(intent.getAction().equals(ACTION_START_FOREGROUND)){
            startForeground(startId, new Notification());
        }
        return Service.START_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Insert value into content provider
     * @param value1 x acceleration
     * @param value2 y acceleration
     * @param value3 z acceleration
     */
    private void insertRecord(float value1, float value2, float value3) {
        if(!firstValueStored){
            firstValueStored = true;
            Intent intent = new Intent();
            intent.setAction("de.msl.startFinishedMovementPlugin");
            sendBroadcast(intent);
        }
        ContentResolver cr = getContentResolver();
        Long time = System.currentTimeMillis();
        ContentValues values = new ContentValues();
        values.put(SleepSQLiteHelper.COLUMN_TIME_FROM, time.toString());
        values.put(SleepSQLiteHelper.COLUMN_TIME_TO, time.toString());
        values.put(SleepSQLiteHelper.COLUMN_VALUE1, value1);
        values.put(SleepSQLiteHelper.COLUMN_VALUE2, value2);
        values.put(SleepSQLiteHelper.COLUMN_VALUE3, value3);
        cr.insert(SleepContentProvider.CONTENT_URI, values);
        Log.i("MOVEMENT_PLUGIN", "Inserted values:" + value1 + " " + value2 + " " + value3);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        //Only store values if accuracy is high
        //if(accuracyHigh) {
            if (event.values.length > 0) {
                if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
                    insertRecord(event.values[0]-offsetX, event.values[1]-offsetY, event.values[2]-offsetZ);
                }
            }
        //}
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        if(sensor.getType()==Sensor.TYPE_LINEAR_ACCELERATION){
            if(accuracy==SensorManager.SENSOR_STATUS_ACCURACY_HIGH){
                accuracyHigh = true;
            }
            else{
                accuracyHigh = false;
            }
        }
    }

}
