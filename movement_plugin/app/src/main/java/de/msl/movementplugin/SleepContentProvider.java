package de.msl.movementplugin;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

/**
 * Created by Andreas on 13.04.2016.
 */
public class SleepContentProvider extends ContentProvider{

    int ctr = 0;

    private SleepSQLiteHelper database;

    private static final int VALUE = 1;
    private static final int VALUE_ID = 2;

    private static final String AUTHORITY = "de.msl.cpmovementplugin";
    private static final String BASE_PATH = SleepSQLiteHelper.TABLE_MOVEMENTPLUGIN_DATA;

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher
                .addURI(AUTHORITY, SleepSQLiteHelper.TABLE_MOVEMENTPLUGIN_DATA, VALUE);
        uriMatcher.addURI(AUTHORITY, SleepSQLiteHelper.TABLE_MOVEMENTPLUGIN_DATA
                + "/#", VALUE_ID);
    }

    // DELETE DATA IS NOT NEEDED

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    // GET TYPE

    @Override
    public String getType(Uri uri) {
        switch(uriMatcher.match(uri)){
            case VALUE:
                return "vnd.android.cursor.dir/vnd." + AUTHORITY + "/" + SleepSQLiteHelper.TABLE_MOVEMENTPLUGIN_DATA;
            case VALUE_ID:
                return "vnd.android.cursor.dir/vnd." + AUTHORITY + "/" + SleepSQLiteHelper.TABLE_MOVEMENTPLUGIN_DATA;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = database.getWritableDatabase();
        long rowId = db.insert(SleepSQLiteHelper.TABLE_MOVEMENTPLUGIN_DATA, "", values);
        ctr++;
        if((rowId > -1)){
            Uri result = ContentUris.withAppendedId(CONTENT_URI, rowId);
            // Only notify Content Resolver about every fifth update to reduce load on receiver side
            if(ctr % 5 == 0)
                getContext().getContentResolver().notifyChange(result,null);
            return  result;
        }
        db.close();
        return null;
    }

    @Override
    public boolean onCreate() {
        database = new SleepSQLiteHelper(getContext());
        return true;
    }

    // QUERY DATA

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        SQLiteDatabase db = database.getWritableDatabase();
        SQLiteQueryBuilder sqlBuilder = new SQLiteQueryBuilder();
        sqlBuilder.setTables(SleepSQLiteHelper.TABLE_MOVEMENTPLUGIN_DATA);

        if(uriMatcher.match(uri) == VALUE_ID){
            sqlBuilder.appendWhere(SleepSQLiteHelper.COLUMN_ID + " = " + uri.getLastPathSegment());
        }
        if(sortOrder == null || "".equals(sortOrder)){
            sortOrder = SleepSQLiteHelper.COLUMN_ID + " ASC";
        }
        Cursor c = sqlBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);

        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;

    }

    // UPDATE DATA IS NOT NEEDED

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
