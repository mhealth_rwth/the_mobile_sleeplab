package de.msl.movementplugin;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Andreas on 13.04.2016.
 */
public class SleepSQLiteHelper extends SQLiteOpenHelper {
    public static final String TABLE_MOVEMENTPLUGIN_DATA = "movementplugindata";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_VALUE1 = "MONITOR_D056044$X_mss_D000068997";
    public static final String COLUMN_VALUE2 = "MONITOR_D056044$Y_mss_D000068997";
    public static final String COLUMN_VALUE3 = "MONITOR_D056044$Z_mss_D000068997";
    public static final String COLUMN_TIME_FROM = "time_from";
    public static final String COLUMN_TIME_TO = "time_to";
    private static final String DATABASE_NAME = "movementplugindata.db";
    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_CREATE =
            "create table " + TABLE_MOVEMENTPLUGIN_DATA + "(" + COLUMN_ID + " integer primary key autoincrement, " + COLUMN_TIME_FROM + " string not null, " + COLUMN_TIME_TO + " string not null, " + COLUMN_VALUE1 + " float not null, " + COLUMN_VALUE2 + " float not null, " + COLUMN_VALUE3 + " float not null);";

    public SleepSQLiteHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION );
    }

    @Override
    public void onCreate(SQLiteDatabase database){
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MOVEMENTPLUGIN_DATA);
        onCreate(db);
    }
}
