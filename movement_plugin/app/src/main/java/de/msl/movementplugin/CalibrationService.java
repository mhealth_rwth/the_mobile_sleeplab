package de.msl.movementplugin;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;

public class CalibrationService extends Service implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor movementSensor;

    private int mode = SensorManager.SENSOR_DELAY_UI;

    public final static String SEND_CALLIBRATION_RESULTS = "de.msl.movementplugin.callibrationresults";

    private ArrayList<Float> calX = new ArrayList<>();
    private ArrayList<Float> calY = new ArrayList<>();
    private ArrayList<Float> calZ = new ArrayList<>();

    private int ctr = 0;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate(){
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int x, int y){
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        movementSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        mode = intent.getIntExtra("mode", SensorManager.SENSOR_DELAY_UI);

        sensorManager.registerListener(this, movementSensor, mode);

        return Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy(){
        sensorManager.unregisterListener(this);
        super.onDestroy();
    }

    /**
     * Callibrates the sensor for the first 100 measurements
     * @param event
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.values.length > 0) {
            if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {

                float newX = event.values[0];
                float newY = event.values[1];
                float newZ = event.values[2];

                // ignore first 50 values (lying phone down)
                if((ctr> 50) && (ctr < 100)){
                    calX.add(newX);
                    calY.add(newY);
                    calZ.add(newZ);
                }
                if(ctr == 100){
                    float sumX = 0.0f;
                    float sumY = 0.0f;
                    float sumZ = 0.0f;
                    float size = (float)calX.size();
                    for(float f : calX){
                        sumX += f;
                    }
                    for(float f : calY){
                        sumY += f;
                    }
                    for(float f : calZ){
                        sumZ += f;
                    }
                    float offsetX = sumX/size;
                    float offsetY = sumY/size;
                    float offsetZ = sumZ/size;

                    broadcastResult(offsetX, offsetY, offsetZ);
                }
                ctr++;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /**
     * Send offset values
     * @param x x offset
     * @param y y offset
     * @param z z offset
     */
    public void broadcastResult(float x, float y, float z) {
        Intent intent = new Intent(SEND_CALLIBRATION_RESULTS);
        intent.putExtra("offsetX", x);
        intent.putExtra("offsetY", y);
        intent.putExtra("offsetZ", z);
        getApplicationContext().sendBroadcast(intent);
    }

}
