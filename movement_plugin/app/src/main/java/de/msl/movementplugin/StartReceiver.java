package de.msl.movementplugin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

/**
 * Created by Andreas on 13.04.2016.
 */
public class StartReceiver extends BroadcastReceiver{

    private static final String TAG = "MOVEMENTPLUGIN RECV";

    @Override
    public void onReceive(Context context, Intent intentIncoming){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent intent1 = new Intent(context.getApplicationContext(),StartReceiver.class);
            context.startForegroundService(intent1);
        }
        Intent intentOutgoing = new Intent();

        String action = intentIncoming.getAction();

        switch (action){
            case "de.msl.startmovementplugin":
                Log.d(TAG, "started");
                intentOutgoing.setClass(context.getApplicationContext(), StartUp.class);
                intentOutgoing.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.getApplicationContext().startActivity(intentOutgoing);
                break;
            case "de.msl.stopmovementplugin":
                Log.d(TAG, "stopped");
                intentOutgoing.setClass(context.getApplicationContext(), GenerateService.class);
                context.stopService(intentOutgoing);
                break;
            default:
                Log.d(TAG, "unknown command");
        }

    }
}
