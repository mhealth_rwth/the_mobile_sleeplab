package de.msl.movementplugin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class StartUp extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Spinner spinnerAccelerometerMode;
    private TextView textViewWarning;

    private ArrayList<String> entries;

    private String accelerometerMode = "SENSOR_DELAY_NORMAL";
    private int mode = SensorManager.SENSOR_DELAY_UI;

    private BroadcastReceiver callibrationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intentIncoming){
            if(intentIncoming.getAction().equals(CalibrationService.SEND_CALLIBRATION_RESULTS)){
                Intent stopServiceIntent = new Intent();
                stopServiceIntent.setClass(getApplicationContext(), CalibrationService.class);
                stopService(stopServiceIntent);
                float x = intentIncoming.getFloatExtra("offsetX", 0.0f);
                float y = intentIncoming.getFloatExtra("offsetY", 0.0f);
                float z = intentIncoming.getFloatExtra("offsetZ", 0.0f);
                finalizeIt(x,y,z);
            }
        }
    };

    Intent callingIntent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up);

        spinnerAccelerometerMode = (Spinner) findViewById(R.id.spinnerAccelerometerMode);
        textViewWarning = (TextView) findViewById(R.id.textViewWarning);

        textViewWarning.setVisibility(View.INVISIBLE);

        spinnerAccelerometerMode.setOnItemSelectedListener(this);

        callingIntent = getIntent();

        IntentFilter intentFilter = new IntentFilter(
                CalibrationService.SEND_CALLIBRATION_RESULTS);


        registerReceiver(callibrationReceiver, intentFilter);


    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        unregisterReceiver(callibrationReceiver);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        accelerometerMode = parent.getItemAtPosition(pos).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void saveSettings(View view){

        textViewWarning.setVisibility(View.VISIBLE);

        mode = SensorManager.SENSOR_DELAY_UI;
        switch (accelerometerMode){
            case "SENSOR_DELAY_NORMAL":
                mode = SensorManager.SENSOR_DELAY_NORMAL;
                break;
            case "SENSOR_DELAY_UI":
                mode = SensorManager.SENSOR_DELAY_UI;
                break;
            case "SENSOR_DELAY_FASTEST":
                mode = SensorManager.SENSOR_DELAY_FASTEST;
                break;
            case "SENSOR_DELAY_GAME":
                mode = SensorManager.SENSOR_DELAY_GAME;
                break;
            default:
                mode= SensorManager.SENSOR_DELAY_NORMAL;
                break;
        }

        Intent startCalibrationIntent = new Intent();
        startCalibrationIntent.putExtra("mode", mode);
        startCalibrationIntent.setClass(getApplicationContext(), CalibrationService.class);
        startService(startCalibrationIntent);
    }

    public void finalizeIt(float offsetX, float offsetY, float offsetZ){
        Intent intentOutgoing = new Intent(GenerateService.ACTION_START_FOREGROUND);
        intentOutgoing.putExtra("offsetX", offsetX);
        intentOutgoing.putExtra("offsetY", offsetY);
        intentOutgoing.putExtra("offsetZ", offsetZ);
        intentOutgoing.putExtra("accelerometerMode",accelerometerMode);

        intentOutgoing.setClass(this, GenerateService.class);
        startService(intentOutgoing);

        StartUp.this.finish();
    }
}
