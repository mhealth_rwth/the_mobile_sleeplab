package de.msl.simplegraphs.utils;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import de.msl.simplegraphs.SimpleGraph;

/**
 * Created by Andreas on 09.06.2016.
 */
public class DataFetcher {

    int quality;

    public static final String finishedTaskIntent = "de.msl.simplegraphs.finished";

    private Context context;
    private ArrayList<String> allPlugins;
    private Intent oldIntent;

    private List<String> dataFormat = new ArrayList<>();

    private long recordId;

    private long startTime = 0;

    private ArrayList<SimpleGraphValue> simpleValues;
    private ArrayList<String> usedValues;

    private Map<String, SimpleGraphValue> resultValues = new TreeMap<>();

    public ArrayList<SimpleGraphValue> getSimpleValues(){
        return simpleValues;
    }

    public DataFetcher(Context context, ArrayList<String> allPlugins, Intent oldIntent, long recordId, ArrayList<String> usedValues){
        this.context = context;
        this.allPlugins = allPlugins;
        this.oldIntent = oldIntent;
        this.recordId = recordId;
        this.usedValues = usedValues;
        this.quality = oldIntent.getIntExtra("quality", 1);
    }

    // fetch data in asynchronous task
    public void start(){
        new DataCalculator().execute();
    }

    private void start2(){
        // Create list with all data fields
        simpleValues = new ArrayList<>();
        for (String plugin : allPlugins) {
            ArrayList<String> allDataFields = oldIntent.getStringArrayListExtra(plugin);
            getData(plugin, allDataFields);
        }
        simpleValues = new ArrayList<>(resultValues.values());
    }

    /**
     * Fetches the data for one certain plugin
     * @param plugin The plugin to fetch the data for
     * @param allDataFields the fields which are available for this plugin
     * @return
     */

    public void getData(String plugin, ArrayList<String> allDataFields){

        dataFormat = new ArrayList<>();
        int ctr = 0;

        for(String s : allDataFields){
            dataFormat.add(ctr, s);
            ctr++;
        }

        Uri uri = Uri.parse("content://de.msl.cpmobilesleeplab/" + plugin + "/" + recordId);

        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();


        // Default entries which are part of every result table

        int timeFromPos = allDataFields.indexOf("time_from");
        int recordPos = allDataFields.indexOf("record");
        int idPos = allDataFields.indexOf("_id");
        int timeToPos = allDataFields.indexOf("time_to");


        long iter = 0;

        while (!cursor.isAfterLast()){
            if(iter %quality == 0) {
                boolean newV = false;
                String timeFrom = cursor.getString(timeFromPos);
                if (startTime == 0) {
                    startTime = Long.valueOf(timeFrom);
                }

                SimpleGraphValue currentGraphValue;
                currentGraphValue = resultValues.get(timeFrom);

                // If now entry with current time value, create new one
                if (currentGraphValue == null) {
                    newV = true;
                    currentGraphValue = new SimpleGraphValue(startTime, Long.valueOf(timeFrom), usedValues);
                }

                // fill graph value
                for (String s : allDataFields) {
                    int pos = allDataFields.indexOf(s);
                    if ((pos != timeFromPos) && (pos != idPos) && (pos != recordPos) && (pos != timeToPos)) {
                        if (usedValues.contains(s)) {
                            int posX = usedValues.indexOf(s);
                            currentGraphValue.setValue(posX, cursor.getString(pos));
                        }
                    }
                }
                currentGraphValue.calculateNumberValues();

                if (newV)
                    resultValues.put(timeFrom, currentGraphValue);
            }
            iter++;
            cursor.moveToNext();
        }
        cursor.close();
    }

    private class DataCalculator extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            start2();
            return null;
        }

        protected void onPostExecute(Void d) {
            context.sendBroadcast(new Intent(finishedTaskIntent));
        }

    }
}
