package de.msl.simplegraphs;

/**
 * Created by andre on 08.03.2016.
 */
public class Utils {
    public static String formatDate(String date){
        String year = date.substring(0,4);
        String month = date.substring(4,6);
        String day = date.substring(6,8);
        String hour = date.substring(9,11);
        String minute = date.substring(11,13);
        String second = date.substring(13,15);

        return day + "." + month + "." + year + " " + hour + ":" + minute + ":" + second;
    }
}