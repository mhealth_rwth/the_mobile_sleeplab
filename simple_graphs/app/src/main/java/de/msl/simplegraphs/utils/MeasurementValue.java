package de.msl.simplegraphs.utils;

/**
 * Created by Andreas on 26.05.2016.
 */
public class MeasurementValue {
    private String kindOfPlugin;
    private String kindOfValue;
    private String valueExtension="";
    private String unit;
    private String location;

    private boolean locationIsRelevant = true;
    private boolean kindOfValueIsRelevant = true;
    private boolean hasExtension = false;

    public MeasurementValue(String wholePlugin){
        String[] fields = wholePlugin.split("_");
        kindOfPlugin = fields[0];
        if(fields[1].contains("$")){
            hasExtension = true;
            String[] subfields = fields[1].split("\\$");
            valueExtension = subfields[1];
            if(subfields[0].equals("ANY")){
                kindOfValueIsRelevant = false;
            }
            kindOfValue = subfields[0];
        }
        else{
            if(fields[1].equals("ANY")){
                kindOfValueIsRelevant = false;
            }
            kindOfValue = fields[1];
        }
        unit = fields[2];
        if(fields[3].equals("ANY")){
            locationIsRelevant = false;
        }
        location = fields[3];
    }


    public String getKindOfPlugin() {
        return kindOfPlugin;
    }

    public String getKindOfValue() {
        return kindOfValue;
    }

    public String getKindOfValueDescription(){
        return MeshLookup.getDescriptionForCode(kindOfValue);
    }

    public String getValueExtension() {
        if(hasExtension)
            return valueExtension;
        return "";
    }

    public String getUnit() {
        return unit;
    }

    public String getLocation() {
        return location;
    }

    public String getLocationDescription(){
        return MeshLookup.getDescriptionForCode(location);
    }

    public boolean isLocationIsRelevant() {
        return locationIsRelevant;
    }

    public boolean isKindOfValueIsRelevant() {
        return kindOfValueIsRelevant;
    }

    public boolean isHasExtension() {
        return hasExtension;
    }

}