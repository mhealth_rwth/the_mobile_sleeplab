package de.msl.simplegraphs.utils;

import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Andreas on 30.06.2016.
 * Stores one point in time of a graph with all possible values which can be displayed
 */
public class SimpleGraphValue {

    private long timeInMs;
    private long timeInMsSinceStart;
    private Date timeStamp;
    private ArrayList<String> values;
    private ArrayList<String> descriptions;

    private ArrayList<Float> numberValues;

    private ArrayList<String> numberValuesDescriptions;

    public SimpleGraphValue(long startTime, long timeInMs, ArrayList<String> descriptions){
        this.timeInMs = timeInMs;
        this.timeInMsSinceStart = timeInMs - startTime;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMs);
        this.timeStamp = calendar.getTime();
        this.descriptions = descriptions;
        values = new ArrayList<>();
        for(int i = 0; i < descriptions.size(); i++){
            values.add("");
        }
    }

    public void setValue(int index, String value){
        if(values.size()>index){
            values.set(index,value);
        }
    }
    public long getTimeInMs() {
        return timeInMs;
    }

    public long getTimeInMsSinceStart() {
        return timeInMsSinceStart;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public ArrayList<String> getValues() {
        return values;
    }

    public ArrayList<String> getDescriptions(){
        return descriptions;
    }

    public ArrayList<String> getNumberValuesDescriptions() {
        return numberValuesDescriptions;
    }

    public ArrayList<Float> getNumberValues() {
        return numberValues;
    }

    // // TODO: 08.07.2016 test if null is okay or if we need "0"
    public String getValue(int index){
        try{
            return values.get(index);
        }
        catch(Exception e){
            return null;
        }
    }

    public float getNumberValue(int index){
        try{
            return numberValues.get(index);
        }
        catch (Exception e){
            return 0f;
        }
    }


    public void calculateNumberValues(){
        numberValues = new ArrayList<>();
        numberValuesDescriptions = new ArrayList<>();

        for(int i = 0; i < values.size(); i++){
            if(NumberUtils.isNumber(values.get(i))){
                numberValues.add(Float.valueOf(values.get(i)));
                numberValuesDescriptions.add(descriptions.get(i));
            }
        }
    }
}