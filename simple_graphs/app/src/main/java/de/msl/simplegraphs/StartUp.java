package de.msl.simplegraphs;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.StringTokenizer;

import de.msl.simplegraphs.utils.MeasurementValue;

public class StartUp extends AppCompatActivity {

    private ArrayList<String> allPlugins;
    private long recordId;
    private Intent firstIntent;

    private ArrayList<String> usedValues;

    private String fieldId = "_id";
    private String fieldRecord = "record";
    private String fieldTimeFrom = "time_from";
    private String fieldTimeTo = "time_to";

    private EditText qualityView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up);

        qualityView = (EditText) findViewById(R.id.qualityView);

        firstIntent = getIntent();

        recordId = getIntent().getLongExtra("recordid", 1466712166624l);
        allPlugins = getIntent().getStringArrayListExtra("allPlugins");

        usedValues = new ArrayList<>();

        usedValues.add(fieldId);
        usedValues.add(fieldRecord);
        usedValues.add(fieldTimeFrom);
        usedValues.add(fieldTimeTo);

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linear_layout_values);

        /*
        TextView textViewGeneral = new TextView(this);
        textViewGeneral.setText("General");

        linearLayout.addView(textViewGeneral);
        */

        if((allPlugins != null) && (allPlugins.size()>0)){
            for(String s : allPlugins){
                TextView tv = new TextView(this);
                tv.setText(s);
                linearLayout.addView(tv);
                ArrayList<String> values = getIntent().getStringArrayListExtra(s);
                if(values != null){
                    for(final String val : values){

                        if((!val.equals("_id")) && (!val.equals("time_from")) && (!val.equals("time_to")) && (!val.equals("record"))) {
                            CheckBox checkBox = new CheckBox(this);
                            MeasurementValue mv = new MeasurementValue(val);
                            checkBox.setText(mv.getKindOfValueDescription() + " (" + mv.getValueExtension() + ") "  + mv.getLocationDescription());
                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if(isChecked){
                                        usedValues.add(val);
                                    }
                                    else{
                                        if(usedValues.contains(val)){
                                            usedValues.remove(val);
                                        }
                                    }
                                }
                            });
                            linearLayout.addView(checkBox);
                        }
                    }
                }
            }
        }
    }

    public void produceSimpleGraph(View view) {
        Intent intent = new Intent(this, SimpleGraphActivity.class);

        for(String plugin : allPlugins){
            ArrayList<String> valuesOfPlugin = firstIntent.getStringArrayListExtra(plugin);
            ArrayList<String> usedValuesOfPlugin = new ArrayList<>();
            for(String value : valuesOfPlugin){
                usedValuesOfPlugin.add(value);
            }
         intent.putExtra(plugin, usedValuesOfPlugin);

        }

        int quality = Integer.valueOf(qualityView.getText().toString());

        intent.putExtra("usedValues", usedValues);
        intent.putExtra("plugins", allPlugins);
        intent.putExtra("recordid", recordId);
        intent.putExtra("quality", quality);

        startActivity(intent);
    }

}
