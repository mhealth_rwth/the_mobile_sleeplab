package de.msl.simplegraphs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;


import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.Arrays;

import de.msl.simplegraphs.utils.DataFetcher;
import de.msl.simplegraphs.utils.MeasurementValue;
import de.msl.simplegraphs.utils.SimpleGraphValue;

public class SimpleGraphActivity extends Activity {

    public static String nonValues[] = {"_id", "record", "time_from", "time_to"};

    // UI elements

    private LineChart lineChart;
    private DataFetcher dataFetcher;
    private LinearLayout layoutValues;
    private LinearLayout layoutViewSettings;

    private EditText editFrom;
    private EditText editTo;
    private CheckBox cbAutoScale;

    // Logic elements

    private boolean autoScale = true;
    private float yFrom;
    private float yTo;

    private ArrayList<String> shownValues;                          // Values which shall be shown
    private ArrayList<String> usedValues;                           // Values which shall be able to be shown

    private ArrayList<ArrayList<Entry>> entryList;
    private ArrayList<String> labels;                               // Contains points of time
    private ArrayList<LineDataSet> dataSets;

    private LineData lineData;                                      // Contains currently shown line data

    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set up UI

        setContentView(R.layout.activity_simple_graph);

        lineChart = (LineChart) findViewById(R.id.chart);
        layoutValues = (LinearLayout) findViewById(R.id.layoutValues);
        layoutViewSettings = (LinearLayout) findViewById(R.id.layoutViewSettings);

        editFrom = (EditText) findViewById(R.id.editFrom);
        editTo = (EditText) findViewById(R.id.editTo);

        try {
            IntentFilter intentFilter = new IntentFilter(DataFetcher.finishedTaskIntent);
            registerReceiver(br, intentFilter);
        }
        catch (Exception e){

        }

        cbAutoScale = (CheckBox) findViewById(R.id.cbAutoScale);
        cbAutoScale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    autoScale = true;
                    lineChart.setAutoScaleMinMaxEnabled(true);
                }
                else{
                    autoScale = false;
                    lineChart.setAutoScaleMinMaxEnabled(false);
                }
            }
        });

        // Create checkboxes for all possible values

        usedValues = getIntent().getStringArrayListExtra("usedValues");
        shownValues = new ArrayList<>(usedValues);


        for (final String value : usedValues) {
            if (!Arrays.asList(nonValues).contains(value)) {
                CheckBox checkBox = new CheckBox(this);
                MeasurementValue mv = new MeasurementValue(value);
                checkBox.setText(mv.getKindOfValueDescription() + " (" + mv.getValueExtension() + ") " + mv.getLocationDescription());
                checkBox.setChecked(true);
                checkBox.setBackgroundColor(getAColor(usedValues.indexOf(value)));
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            shownValues.add(value);
                        } else {
                            if (shownValues.contains(value)) {
                                shownValues.remove(value);
                            }
                        }
                    }
                });
                layoutValues.addView(checkBox);
            }
        }

        // Fetch the data for all possible values before storing it

        dataFetcher = new DataFetcher(this.getApplicationContext(), getIntent().getStringArrayListExtra("plugins"), getIntent(), getIntent().getLongExtra("recordid", 0), usedValues);

        progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.show();

        dataFetcher.start();

    }

    @Override
    public void onStop(){
        unregisterReceiver(br);
        super.onStop();
    }

    public void prepareGraph() {

        // Load Values from DataFetcher
        ArrayList<SimpleGraphValue> graphValues = dataFetcher.getSimpleValues();

        entryList = new ArrayList<>();

        for (int i = 0; i < usedValues.size(); i++) {
            entryList.add(new ArrayList<Entry>());
        }


        labels = new ArrayList<>();

        int ctr = 0;

        for (SimpleGraphValue graphValue : graphValues) {

            boolean validValue = false;

            // Add all numeric values to Entries for the Graph
            for (int i = 0; i < usedValues.size(); i++) {
                String value = graphValue.getValue(i);
                if (NumberUtils.isNumber(value)) {
                    validValue = true;
                    float y = 1f * Float.valueOf(value);
                    int x = (int) graphValue.getTimeInMsSinceStart();
                    Entry e = new Entry(y, ctr);
                    entryList.get(i).add(e);
                }
            }

            // Only add label if at least one of the destined values is a numeric
            if (validValue) {
                labels.add("" + graphValue.getTimeStamp());
            }

            ctr++;

        }

        dataSets = new ArrayList<>();

        int c = 0;

        for (ArrayList<Entry> el : entryList) {
            String val = usedValues.get(entryList.indexOf(el));
            String label = "";
            if(!Arrays.asList(nonValues).contains(val)){
                MeasurementValue mv = new MeasurementValue(val);
                label = mv.getKindOfValueDescription() + " (" + mv.getValueExtension() + ") " + mv.getLocationDescription();
            }
            LineDataSet curDataSet = new LineDataSet(entryList.get(c), label);
            curDataSet.setColor(getAColor(c));
            curDataSet.setCircleColor(getAColor(c));
            curDataSet.setDrawCircles(false);
            curDataSet.setDrawValues(false);
            dataSets.add(curDataSet);
            c++;
        }


        lineData = new LineData(labels);

        for(int i = 4; i < dataSets.size(); i++){
            lineData.addDataSet(dataSets.get(i));
        }

        lineChart.setData(lineData);

        lineChart.setDescription("Sleeplab");

    }

    /**
     * Is called on refresh.
     * Creates new graph with selected values
     * @param view
     */
    public void refreshGraph(View view) {

        lineChart.clear();
        lineData = new LineData(labels);
        for (String val : shownValues) {
            int id = usedValues.indexOf(val);
            lineData.addDataSet(dataSets.get(id));
        }
        lineChart.setData(lineData);
        lineChart.setDescription("Sleeplab");

        if(!autoScale) {
            yFrom = Float.valueOf(editFrom.getText().toString());
            yTo = Float.valueOf(editTo.getText().toString());

            YAxis leftAxis = lineChart.getAxisLeft();
            YAxis rightAxis = lineChart.getAxisRight();

            leftAxis.setAxisMaxValue(yTo);
            leftAxis.setAxisMinValue(yFrom);
            rightAxis.setAxisMaxValue(yTo);
            rightAxis.setAxisMinValue(yFrom);
        }
        else{
            lineChart.setAutoScaleMinMaxEnabled(true);
        }

        lineChart.refreshDrawableState();

    }

    /**
     * Makes sure that only one color is returned for each line inside a graph
     * @param index counter for lines
     * @return index of destined color
     */
    public int getAColor(int index) {
        switch ((index%15)) {
            case 0:
                return Color.rgb(0,255,255);
            case 1:
                return Color.rgb(127,255,212);
            case 2:
                return Color.rgb(0,0,255);
            case 3:
                return Color.rgb(138,43,226);
            case 4:
                return Color.rgb(165,42,42);
            case 5:
                return Color.rgb(255,127,80);
            case 6:
                return Color.rgb(0,100,0);
            case 7:
                return Color.rgb(255,140,0);
            case 8:
                return Color.rgb(255,215,0);
            case 9:
                return Color.rgb(124,252,0);
            case 10:
                return Color.rgb(255,0,255);
            case 11:
                return Color.rgb(123,104,238);
            case 12:
                return Color.rgb(25,25,112);
            case 13:
                return Color.rgb(205,133,63);
            case 14:
                return Color.rgb(70,130,180);
            case 15:
                return Color.rgb(0,128,128);
            default:
                return Color.BLACK;
        }
    }


    BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(DataFetcher.finishedTaskIntent)){
                prepareGraph();
                progress.dismiss();
            }
        }
    };

    // TODO: 08.07.2016  add a function which only shows a certain time range
    // TODO: 08.07.2016  add a function which marks points which lie above / below a certain threshold
    // TODO: 08.07.2016  if phone is turned around graph in fullscreen

}
