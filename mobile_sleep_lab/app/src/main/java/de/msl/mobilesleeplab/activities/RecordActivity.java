package de.msl.mobilesleeplab.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.msl.mobilesleeplab.R;
import de.msl.mobilesleeplab.data.RecordDataSource;
import de.msl.mobilesleeplab.model.Observation;
import de.msl.mobilesleeplab.model.Record;
import de.msl.mobilesleeplab.plugins.MonitorPlugin;
import de.msl.mobilesleeplab.plugins.PresentationPlugin;
import de.msl.mobilesleeplab.plugins.ProcessingPlugin;
import de.msl.mobilesleeplab.utils.SleepManager;
import de.msl.mobilesleeplab.utils.Utils;

/**
 * Activity for visualizing one records. Receives a list for valid monitor plugins for further inspections
 */
public class RecordActivity extends AppCompatActivity {

    TextView recordId;
    TextView dateFrom;
    TextView dateTo;
    TextView user;
    TextView description;
    TextView usedPlugins;

    List<Observation> observationList;

    ProcessingPlugin currentProcessingPlugin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        recordId = (TextView) findViewById(R.id.record_id);
        dateFrom = (TextView) findViewById(R.id.date_from);
        dateTo = (TextView) findViewById(R.id.date_to);
        user = (TextView) findViewById(R.id.User);
        description = (TextView) findViewById(R.id.description);
        usedPlugins = (TextView) findViewById(R.id.usedPlugins);

        Intent intent = getIntent();
        final long record_id = intent.getLongExtra("record_id", 0);

        RecordDataSource recordDataSource = new RecordDataSource();
        final Record record = recordDataSource.getRecordById(record_id);

        // Provide Information about record

        recordId.setText(""+record.getId());
        try {
            dateFrom.setText(Utils.formatDate(record.getDateFrom()));
        }
        catch (Exception e){

        }
        try {

            dateTo.setText(Utils.formatDate(record.getDateTo()));
        }
        catch (Exception e) {

        }

        try {
            user.setText(record.getUser().toString());
        }
        catch (Exception e){}

        try {
            description.setText(record.getDescription());
        }
        catch (Exception e){}

        try{
            ArrayList<MonitorPlugin> plugins = record.getUsedMonitorPlugins();
            String pluginNames = "";
            for(MonitorPlugin plugin : plugins){
                pluginNames += plugin.getName() + "\n";
            }
            usedPlugins.setText(pluginNames);
        }
        catch (Exception e){}


        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.buttonsLayout);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 10, 0, 0);

        // Add buttons for presentation plugins
        if(SleepManager.getInstance().getPluginManager().getPresentationPlugins() != null) {
            for (final PresentationPlugin presentationPlugin : SleepManager.getInstance().getPluginManager().getPresentationPlugins()) {
                if (SleepManager.getInstance().getRecordManager().recordFitsForPresentation(record, presentationPlugin)) {
                    String text = presentationPlugin.getName();
                    Button b = new Button(this);
                    b.setBackgroundColor(Color.parseColor("#b3c0d3"));
                    b.setText(text);
                    b.setLayoutParams(params);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.putExtra("recordid", record_id);
                            SleepManager.getInstance().getRecordManager().getPluginsForRecordWithProcessing(record, intent);
                            intent.setAction(presentationPlugin.getStartCommand());
                            RecordActivity.this.startActivity(intent);
                        }
                    });
                    linearLayout.addView(b);
                }
            }
        }
        // Add buttons for processing plugins
        if(SleepManager.getInstance().getPluginManager().getProcessingPlugins().size()>0) {
            for (final ProcessingPlugin processingPlugin : SleepManager.getInstance().getPluginManager().getProcessingPlugins()) {
                if ((SleepManager.getInstance().getRecordManager().recordFitsForProcessing(record, processingPlugin)) &&(SleepManager.getInstance().getPluginManager().getActiveProcessingPlugins().contains(processingPlugin))) {
                    String text = processingPlugin.getName();
                    Button b = new Button(this);
                    b.setText(text);
                    b.setLayoutParams(params);
                    b.setBackgroundColor(Color.parseColor("#b3c0d3"));
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.putExtra("recordid", record_id);
                            processingPlugin.startPlugin(RecordActivity.this, SleepManager.getInstance().getRecordManager().getRecord(record_id));
                            currentProcessingPlugin = processingPlugin;
                            SleepManager.getInstance().getRecordManager().getPluginsForRecord(record, intent);
                            intent.setAction(processingPlugin.getStartCommand());
                            RecordActivity.this.sendBroadcast(intent);
                            IntentFilter intentFilter = new IntentFilter();
                            intentFilter.addAction("de.msl.processingplugin.finished");
                            RecordActivity.this.registerReceiver(finishReceiver, intentFilter);
                        }
                    });
                    linearLayout.addView(b);
                }
            }
        }
    }

    BroadcastReceiver finishReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action){
                case "de.msl.processingplugin.finished":
                    currentProcessingPlugin.stopPlugin(context);
                    Log.d("Record Activity", "Received stop");
            }
        }
    };
}
