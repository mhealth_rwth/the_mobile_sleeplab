package de.msl.mobilesleeplab.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

import de.msl.mobilesleeplab.model.Record;
import de.msl.mobilesleeplab.plugins.MonitorPlugin;

/**
 * Created by Andreas on 27.04.2016.
 * Manager which controls Ongoing Measurements
 */
public class MeasurementManager {

    private static MeasurementManager managerInstance = null;

    private static boolean ongoingMeasurement;

    protected static MeasurementManager getInstance(){
        if(managerInstance == null){
            managerInstance = new MeasurementManager();
        }
        return managerInstance;
    }

    /**
     * Checks whether there is an ongoind measurement or not
     * @return true if ongoing measurement
     */
    public boolean isOngoingMeasurement(){
        return ongoingMeasurement;
    }

    /**
     * Starts a new Measurement
     * @param context application context
     * @param description description for new record
     */
    public void startNewMeasurement(Context context, String description){
        Record newRecord = SleepManager.getInstance().getRecordManager().prepareNewRecord(description);

        ArrayList<MonitorPlugin> usedPlugins = SleepManager.getInstance().getPluginManager().getActiveMonitorPlugins();

        if(usedPlugins.size()>0){
            for(MonitorPlugin p : usedPlugins){
                p.createDataSource();
                p.getDataSource().setDbHelper(SleepManager.getInstance().getPluginManager().getCustomSQLHelper());
                p.startPlugin(context, newRecord);
            }
        }
        ongoingMeasurement = true;
        storeStatusToPreferences(context);
    }

    /**
     * Stops ongoind measurements
     * @param context application context
     */
    public void stopMeasurement(Context context){
        ArrayList<MonitorPlugin> usedPlugins = SleepManager.getInstance().getPluginManager().getActiveMonitorPlugins();
        if(usedPlugins.size()>0){
            for(MonitorPlugin p : usedPlugins){
                p.stopPlugin(context);
            }
        }
        SleepManager.getInstance().getRecordManager().finishRecord();
        ongoingMeasurement = false;
        storeStatusToPreferences(context);
    }

    /**
     * Stores the current application status (ongoing measurement) to preferences
     * @param context application context
     */
    private void storeStatusToPreferences(Context context){
        SharedPreferences sp = context.getSharedPreferences("MSLPREF", context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        ed.putBoolean("ONGOING_MEASUREMENT", ongoingMeasurement);
        ed.commit();
    }

    /**
     * loads the current application status (ongoing measurement) from preferences
     * @param context application context
     */
    protected void loadStatusFromPreferences(Context context){
        SharedPreferences sp = context.getSharedPreferences("MSLPREF", context.MODE_PRIVATE);
        if(sp.getBoolean("ONGOING_MEASUREMENT", false)){
            ongoingMeasurement = true;
        }
        else {
            ongoingMeasurement = false;
        }
    }
}
