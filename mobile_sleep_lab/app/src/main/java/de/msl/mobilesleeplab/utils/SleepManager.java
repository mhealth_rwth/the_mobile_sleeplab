package de.msl.mobilesleeplab.utils;

import android.content.Context;
import android.content.SharedPreferences;

import de.msl.mobilesleeplab.data.MslContentProvider;

/**
 * Created by Andreas on 27.04.2016.
 * Central Manager
 */
public class SleepManager {

    private static SleepManager managerInstance = null;

    private static PluginManager pluginManager;

    private static RecordManager recordManager;

    private static UserManager userManager;

    private static MeasurementManager measurementManager;

    // Initialization stuff

    public SleepManager(){
        this.pluginManager = PluginManager.getInstance();
        this.recordManager = RecordManager.getInstance();
        this.measurementManager = MeasurementManager.getInstance();
        this.userManager = UserManager.getInstance();
    }

    /**
     * Returns only instance of SleepManager
     * @return Instance of SleepManager
     */
    public static SleepManager getInstance(){
        if(managerInstance==null){
            managerInstance = new SleepManager();
        }
        return managerInstance;
    }

    // Getters & Setters

    /**
     * Returns the RecordManager
     * @return instance of RecordManager
     */
    public static RecordManager getRecordManager() {
        return recordManager;
    }

    /**
     * Returns the PluginManager
     * @return instance of PluginManager
     */
    public static PluginManager getPluginManager() {
        return pluginManager;
    }

    /**
     * Returns the MeasurementManager
     * @return instance of MeasurementManager
     */
    public static MeasurementManager getMeasurementManager(){
        return measurementManager;
    }

    /**
     * Returns UserManager
     * @return instance of UserManager
     */
    public static UserManager getUserManager(){
        return userManager;
    }

    // Methods

    /**
     * Initializes SleepManager
     * @param context application context
     */
    public void init(Context context){
        // Init Plugins
        pluginManager.detectAllPlugins(context);
        pluginManager.init(context);
        SharedPreferences preferences = context.getSharedPreferences("MSLPREF", context.MODE_PRIVATE);
        pluginManager.loadPluginsFromSettings(preferences);

        // Init Content Provider
        MslContentProvider.init();

        // Init Record Manager
        recordManager.init();

        // Init Measurement Manager
        measurementManager.loadStatusFromPreferences(context);

        // Init User Manager
        userManager.init();
    }


}
