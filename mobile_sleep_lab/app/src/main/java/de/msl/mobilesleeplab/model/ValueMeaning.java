package de.msl.mobilesleeplab.model;

import de.msl.mobilesleeplab.utils.MeshLookup;

/**
 * Created by Andreas on 26.05.2016.
 * This class is a more intuitive representation of the column names, provided by plugins
 */
public class ValueMeaning {
    private String kindOfPlugin;
    private String kindOfValue;
    private String valueExtension="";
    private String unit;
    private String location;

    private boolean locationIsRelevant = true;
    private boolean kindOfValueIsRelevant = true;
    private boolean hasExtension = false;

    /**
     *
     * @param wholePlugin String representation of the colum name
     */
    public ValueMeaning(String wholePlugin){
        String[] fields = wholePlugin.split("_");
        kindOfPlugin = fields[0];
        if(fields[1].contains("$")){
            hasExtension = true;
            String[] subfields = fields[1].split("\\$");
            valueExtension = subfields[1];
            if(subfields[0].equals("ANY")){
                kindOfValueIsRelevant = false;
            }
            kindOfValue = subfields[0];
        }
        else{
            if(fields[1].equals("ANY")){
                kindOfValueIsRelevant = false;
            }
            kindOfValue = fields[1];
        }
        unit = fields[2];
        if(fields[3].equals("ANY")){
            locationIsRelevant = false;
        }
        location = fields[3];
    }

    /**
     * Returns the kind of plugin
     * @return kind of plugin
     */
    public String getKindOfPlugin() {
        return kindOfPlugin;
    }

    /**
     * Returns the kind of value as MeSH code
     * @return kind of value
     */
    public String getKindOfValue() {
        return kindOfValue;
    }

    /**
     * Returns the kind of value in words
     * @return kind of value
     */
    public String getKindOfValueDescription(){
        return MeshLookup.getDescriptionForCode(kindOfValue);
    }

    /**
     * Returns the value extension of this value
     * @return value extension
     */
    public String getValueExtension() {
        return valueExtension;
    }

    /**
     * Returns the unit of the value
     * @return unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Returns the value location as MeSH code
     * @return location
     */
    public String getLocation() {
        return location;
    }

    /**
     * Returns the value location in words
     * @return location
     */
    public String getLocationDescription(){
        return MeshLookup.getDescriptionForCode(location);
    }

    /**
     * Returns whether the location is relevant or not
     * @return true if location relevant
     */
    public boolean isLocationIsRelevant() {
        return locationIsRelevant;
    }

    /**
     * Returns whether the kind of value is relevant or not
     * @return true if kind of value relevant
     */
    public boolean isKindOfValueIsRelevant() {
        return kindOfValueIsRelevant;
    }

    /**
     * Returns whether the kind of value has an extension or not
     * @return true if kind of value has extension
     */
    public boolean isHasExtension() {
        return hasExtension;
    }

}