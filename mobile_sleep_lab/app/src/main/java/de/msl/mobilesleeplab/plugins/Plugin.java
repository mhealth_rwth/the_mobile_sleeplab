package de.msl.mobilesleeplab.plugins;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;

import de.msl.mobilesleeplab.model.Record;

/**
 * Created by Andreas on 04.03.2016.
 * Abstract representation of plugin
 */
public abstract class Plugin {

    private String name;
    private int version;

    private String startCommand;
    private String stopCommand;

    private String description;

    /**
     * @param info Info Message which is provided by the plugin's manifest
     */
    public Plugin(ResolveInfo info, boolean generatesData){

        Bundle bundle = info.serviceInfo.metaData;

        String pluginName = bundle.getString("pluginName");
        int pluginVersion = bundle.getInt("pluginVersion");
        String description = bundle.getString("description");
        String startCommand = bundle.getString("startCommand");
        String stopCommand = bundle.getString("stopCommand");

        this.name = pluginName;
        this.version = pluginVersion;
        this.startCommand = startCommand;
        this.stopCommand = stopCommand;

        this.description = description;

    }

    //////////////////////////////////////////////////////////////////////////////
    //          Getter and Setter
    //////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the stop command for the plugin
     * @return stop command
     */
    public String getStopCommand() {
        return stopCommand;
    }

    /**
     * Returns the start command for the plugin
     * @return start command
     */
    public String getStartCommand() {
        return startCommand;
    }

    /**
     * Returns the version of the plugin
     * @return plugin version
     */
    public int getVersion() {
        return version;
    }

    /**
     * Sets the version of the plugin
     * @param version new plugin version
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * Returns the plugin name
     * @return plugin name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the plugin
     * @param name plugin name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the description of the plugin
     * @return plugin description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description of the plugin
     * @param description plugin description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    //////////////////////////////////////////////////////////////////////////////
    //          Start stop methods
    //////////////////////////////////////////////////////////////////////////////

    /**
     * Starts the plugin
     * @param context application context
     * @param record Record to apply the new values to
     */
    public void startPlugin(Context context, Record record) {
        Intent intent = new Intent();
        intent.setAction(startCommand);
        context.sendBroadcast(intent);
    }

    /**
     * Stops the plugin
     * @param context application context
     */
    public void stopPlugin(Context context) {
        Intent intent = new Intent();
        intent.setAction(stopCommand);
        context.sendBroadcast(intent);
    }

}
