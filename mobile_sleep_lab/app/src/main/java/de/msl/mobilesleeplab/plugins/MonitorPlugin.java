package de.msl.mobilesleeplab.plugins;

import android.content.Context;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.msl.mobilesleeplab.communication.PluginObserver;
import de.msl.mobilesleeplab.data.CustomDataSource;
import de.msl.mobilesleeplab.data.CustomSQLHelper;
import de.msl.mobilesleeplab.data.ProvidedInfo;
import de.msl.mobilesleeplab.model.Observation;
import de.msl.mobilesleeplab.model.Record;
import de.msl.mobilesleeplab.utils.SleepManager;

/**
 * Created by Andreas on 11.02.16.
 * Representation of monitor plugins
 */
public class MonitorPlugin extends Plugin{

    private Uri contentUri;
    private PluginObserver pluginObserver;
    private CustomSQLHelper sqlHelper = null;
    private CustomDataSource dataSource = null;

    private int nrOfInts;
    private int nrOfFloats;
    private int nrOfStrings;

    private ArrayList<String> intMeaning;
    private ArrayList<String> floatMeaning;
    private ArrayList<String> stringMeaning;

    private ProvidedInfo providedInfo;

    private String TAG = "PLUGIN";

    private boolean active;

    /**
     * @param info Info Message which is provided by the plugin's manifest
     */
    public MonitorPlugin(ResolveInfo info) {
        super(info, true);

        Bundle bundle = info.serviceInfo.metaData;

        String contentProvider = bundle.getString("contentProvider");
        String contentUri = bundle.getString("contentUri");
        Uri uri = Uri.parse("content://" + contentProvider + "/" + contentUri);

        this.contentUri = uri;

        ArrayList<String> intMeaning = new ArrayList<>();
        ArrayList<String> floatMeaning = new ArrayList<>();
        ArrayList<String> stringMeaning = new ArrayList<>();

        int nrOfInts = bundle.getInt("nrOfInts");
        int nrOfFloats = bundle.getInt("nrOfFloats");
        int nrOfStrings = bundle.getInt("nrOfStrings");
        String intMeanings[] = bundle.getString("meaningOfInts").split(";");
        String floatMeanings[] = bundle.getString("meaningOfFloats").split(";");
        String stringMeanings[] = bundle.getString("meaningOfStrings").split(";");


        if (nrOfInts > 0) {
            Collections.addAll(intMeaning, intMeanings);
        }
        if (nrOfFloats > 0) {
            Collections.addAll(floatMeaning, floatMeanings);
        }
        if (nrOfStrings > 0) {
            Collections.addAll(stringMeaning, stringMeanings);
        }

        this.nrOfInts = nrOfInts;
        this.nrOfFloats = nrOfFloats;
        this.nrOfStrings = nrOfStrings;
        this.intMeaning = intMeaning;
        this.floatMeaning = floatMeaning;
        this.stringMeaning = stringMeaning;
        this.providedInfo = new ProvidedInfo(super.getName(), getMeanings(), nrOfInts,  nrOfFloats,  nrOfStrings);


    }

    /////////////////////////////////////////////////////////////////////////////
    //              Getter and Setter
    /////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the CustomDatasource of this MonitorPlugin
     * @return
     */
    public CustomDataSource getDataSource(){
        return dataSource;
    }

    /**
     * Returns whether the plugin is active or not
     * @return true if plugin is active
     */
    public boolean isActive(){
        return active;
    }

    /**
     * Stores a new received observation to the database
     * @param time_from start time of observation
     * @param time_to end time of observation
     * @param ints list of integers
     * @param floats list of floats
     * @param strings list of strings
     * @param context application context
     */
    public void addValue(String time_from, String time_to, ArrayList<Integer> ints, ArrayList<Float> floats, ArrayList<String> strings, Context context) {
        try {
            dataSource.open();
            dataSource.createValue(time_from, time_to, ints, floats, strings, context, getName());
        } catch (SQLException e) {
            Log.d(TAG, "Error on Adding values to DB");
        }
    }

    /**
     * Returns all Observations which are stored for this plugin
     * @return list of all observations
     */
    public List<Observation> getAllValues() {
        List<Observation> observations = null;
        try {
            dataSource.open();
            observations = dataSource.getAllValues();
            Log.d(TAG + " " + this.getName(), "" + observations.size());
        } catch (SQLException e) {
            Log.d(TAG + " " + this.getName(), "Error on Getting values from DB");
        }
        return observations;
    }




    ///////////////////////////////////////////////////////////////////////////////
    //          Database stuff
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new DataSource for the MonitorPlugin
     */
    public void createDataSource() {

        ArrayList<String> meanings = new ArrayList<>();
        if (this.getIntMeaning().size() > 0) {
            for (String meaning : this.getIntMeaning()) {
                meanings.add(meaning);
            }
        }
        if (this.getFloatMeaning().size() > 0) {
            for (String meaning : this.getFloatMeaning()) {
                meanings.add(meaning);
            }
        }
        if (this.getStringMeaning().size() > 0) {
            for (String meaning : this.getStringMeaning()) {
                meanings.add(meaning);
            }
        }

        if (dataSource == null) {
            dataSource = new CustomDataSource(this.getNrOfInts(), this.getNrOfFloats(), this.getNrOfStrings(), this.getName(), meanings);
        }
    }


    ///////////////////////////////////////////////////////////////////////////////
    //             Observer
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the PluginObserver for the Plugin
     * @param handler Handler for the Observer
     * @param context Application context
     * @return new created observer
     */
    public PluginObserver getObserver(Handler handler, Context context) {
        pluginObserver = new PluginObserver(handler, context, this.getIntMeaning(), this.getFloatMeaning(), this.getStringMeaning(), this);
        return pluginObserver;
    }

    /**
     * Registers the Plugin's ContentObserver
     * @param context application context
     */
    public void registerObserver(Context context) {

        context.getContentResolver().registerContentObserver(contentUri, true, getObserver(new Handler(), context));
    }

    /**
     * unregisters the plugin's ContentObserver
     * @param context aplication context
     */
    public void unregisterObserver(Context context) {
        try {
            context.getContentResolver().unregisterContentObserver(pluginObserver);
        }
        catch (Exception ex){

        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    //          Activation and deactivation
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Activate the plugin
     */
    public void activatePlugin(){
        this.active = true;
        SleepManager.getInstance().getPluginManager().addDataInfo(this.getProvidedInfo());
        createDataSource();
    }

    /**
     * Deactivate the plugin
     */
    public void deactivatePlugin(){
        this.active = false;
    }



    //////////////////////////////////////////////////////////////////////////////
    //          Start stop methods
    //////////////////////////////////////////////////////////////////////////////

    /**
     * Start the Plugin
     * @param context application context
     * @param record Record to apply the new values to
     */
    public void startPlugin(Context context, Record record) {
        super.startPlugin(context, record);
        registerObserver(context);
    }

    /**
     * Stop the Plugin
     * @param context application context
     */
    public void stopPlugin(Context context) {
        super.stopPlugin(context);
        unregisterObserver(context);
    }

    /**
     * Returns the infos provided by the plugin
     * @return provided Information of the monitor plugin
     */
    public ProvidedInfo getProvidedInfo() {
        return providedInfo;
    }

    /**
     * Returns all database columns for the MonitorPlugin
     * @return List of database column names
     */
    public ArrayList<String> getAllDataColums(){
        ArrayList<String> result = new ArrayList<>();
        result.add("_id");
        result.add("record");
        result.add("time_from");
        result.add("time_to");
        result.addAll(providedInfo.getCOLUMNS());
        return result;
    }

    /**
     * Returns the Meaning of Strings
     * @return List with meaning of Strings
     */
    public ArrayList<String> getStringMeaning() {
        return stringMeaning;
    }

    /**
     * Returns the Meaning of Floats
     * @return List with meaning of Floats
     */
    public ArrayList<String> getFloatMeaning() {
        return floatMeaning;
    }

    /**
     * Returns the Meaning of Ints
     * @return List with meaning of Ints
     */
    public ArrayList<String> getIntMeaning() {
        return intMeaning;
    }

    /**
     * Returns the number of Strings
     * @return number of Strings
     */
    public int getNrOfStrings() {
        return nrOfStrings;
    }

    /**
     * Returns the number of Floats
     * @return number of Floats
     */
    public int getNrOfFloats() {
        return nrOfFloats;
    }

    /**
     * Returns the number of Ints
     * @return number of Ints
     */
    public int getNrOfInts() {
        return nrOfInts;
    }

    /**
     * Returns a list of all meanings
     * @return list with meanings of all values
     */
    public ArrayList<String> getMeanings(){
        ArrayList<String> meanings = new ArrayList<>();
        if (intMeaning.size() > 0) {
            for (String meaning : intMeaning) {
                meanings.add(meaning);
            }
        }
        if (floatMeaning.size() > 0) {
            for (String meaning : floatMeaning) {
                meanings.add(meaning);
            }
        }
        if (stringMeaning.size() > 0) {
            for (String meaning : stringMeaning) {
                meanings.add(meaning);
            }
        }
        return meanings;
    }

}
