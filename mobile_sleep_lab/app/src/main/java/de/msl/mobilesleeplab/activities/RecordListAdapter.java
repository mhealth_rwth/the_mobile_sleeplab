package de.msl.mobilesleeplab.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.msl.mobilesleeplab.R;
import de.msl.mobilesleeplab.model.Record;
import de.msl.mobilesleeplab.utils.Utils;

/**
 * Created by andi on 04.02.16.
 */

/**
 * Adapter vor visualizing certain record details in record list
 */
public class RecordListAdapter extends ArrayAdapter<Record> {

    private final Context context;
    private final List<Record> itemsArrayList;

    public RecordListAdapter(Context context, List<Record> itemsArrayList) {
        super(context, R.layout.record_list_item, itemsArrayList);
        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    public List<Record> getItemsArrayList(){
        return itemsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View elementView = inflater.inflate(R.layout.record_list_item, parent, false);

        parent.setBackgroundResource(R.drawable.background);

        TextView dateFromView = (TextView) elementView.findViewById(R.id.record_list_item_from);
        TextView dateToView = (TextView) elementView.findViewById(R.id.record_list_item_to);
        TextView descriptionView = (TextView) elementView.findViewById(R.id.record_list_item_description);

        ImageView imageView = (ImageView) elementView.findViewById(R.id.record_list_item_image);

        dateFromView.setText(Utils.formatDate(itemsArrayList.get(position).getDateFrom()));
        try {
            dateToView.setText(Utils.formatDate(itemsArrayList.get(position).getDateTo()));
        }
        catch (Exception ex){
            dateToView.setText(itemsArrayList.get(position).getDateTo());
        }

        String comDescription = "";
        comDescription += "User: " + itemsArrayList.get(position).getUser().toString() + "\n";
        comDescription += "Description: " + itemsArrayList.get(position).getDescription();
        descriptionView.setText(comDescription);

        imageView.setImageResource(R.drawable.icon_ready_50);

        return elementView;
    }


}