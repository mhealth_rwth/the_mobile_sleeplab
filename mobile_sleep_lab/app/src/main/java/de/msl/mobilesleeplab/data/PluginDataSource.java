package de.msl.mobilesleeplab.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;

import de.msl.mobilesleeplab.plugins.Plugin;
import de.msl.mobilesleeplab.utils.SleepManager;

/**
 * Created by Andreas on 03.03.2016.
 * Datasource for a table which stores information about installed plugins
 */
public class PluginDataSource {

    private SQLiteDatabase database;
    private CustomSQLHelper databaseHelper;

    public CustomSQLHelper getDbHelper() {
        return databaseHelper;
    }

    public void setDbHelper(CustomSQLHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public PluginDataSource(){
        databaseHelper = SleepManager.getInstance().getPluginManager().getCustomSQLHelper();
    }

    public void open() throws SQLException {
        database = databaseHelper.getWritableDatabase();
    }

    public void close(){

    }

    /**
     * Register a new Plugin to the database
     * @param plugin Plugin to register
     */
    public void registerPlugin(Plugin plugin){
        try{
            open();
            String command = "insert into plugins(name, version) values ('";
            command += plugin.getName() + "', ";
            command += plugin.getVersion() + ");";
            database.execSQL(command);
            close();
        }
        catch (Exception e){

        }
    }

    /**
     * Update information about a Plugin
     * @param plugin Plugin to be changed
     */
    public void updateRecord(Plugin plugin){
        try{
            open();
            String command = "update plugins set version = " + plugin.getVersion() +
                    " where name=='" + plugin.getName() + "'";
            database.execSQL(command);
            close();
        }
        catch (Exception e){

        }
    }

    /**
     * Tries to get an old version of the respective plugin
     * @param plugin Plugin to check
     * @return version of installed Plugin
     */
    public int getOldVersion(Plugin plugin){
        try {
            open();
            Cursor c = database.rawQuery("select version from plugins where name == '" + plugin.getName() + "'", null);
            c.moveToFirst();
            int version = c.getInt(0);
            close();
            return version;
        }
        catch(Exception e){
            close();
            return 0;
        }
    }

    /**
     * Drops the table
     * @param plugin
     */
    public void dropTable(Plugin plugin){
        try{
            open();
            //database.execSQL("DROP TABLE IF EXISTS " + plugin.getProvidedInfo().getTABLENAME());
            database.execSQL("DROP TABLE IF EXISTS " + plugin.getName());
            close();
        }
        catch(Exception e){
            close();
        }
    }

}
