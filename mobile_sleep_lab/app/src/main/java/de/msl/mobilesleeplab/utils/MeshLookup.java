package de.msl.mobilesleeplab.utils;

/**
 * Created by Andreas on 26.05.2016.
 * Dictionary for a mapping between MeSH ids and word descriptions
 */
public class MeshLookup {

    /**
     * Returns the description for a certain MeSH code
     * @param code MeSH code
     * @return desription of MeSH item
     */
    public static String getDescriptionForCode(String code){

        switch(code){

            // Locations

            case "D001132":
                return "Arm";
            case "D005542":
                return "Forearm";
            case "D006225":
                return "Hand";
            case "D005385":
                return "Fingers";
            case "D014953":
                return "Wrist";
            case "D001940":
                return "Breast";
            case "D006257":
                return "Head";
            case "D001513":
                return "Beds";
            case "D000068997":
                return "Smart Phones";
            case "D010362":
                return "Patients' Rooms";

            // Examinations

            case "D009068":
                return "Movement";
            case "D004569":
                return "Electroencephalography";
            case "D058256":
                return "Brain Waves";
            case "D000513":
                return "Alpha Rythm";
            case "D001611":
                return "Beta Rythm";
            case "D003700":
                return "Delta Rythm";
            case "D065818":
                return "Gamma Rythm";
            case "D013826":
                return "Theta Rythm";
            case "D006339":
                return "Heart Rate";
            case "D056152":
                return "Respiratory Rate";
            case "D001831":
                return "Body Temperature";
            case "D001794":
                return "Blood Pressure";
            case "D008029":
                return "Lighting";
            case "D013696":
                return "Temperature";
            case "D006813":
                return "Humidity";
            case "D056044":
                return "Actigraphy";
            case "D014831":
                return "Voice";
            case "D013016":
                return "Sound";
            case "D009622":
                return "Noise";
            case "D012913":
                return "Snoring";
            case "D003371":
                return "Cough";

            default:
                return "unknown";

        }
    }
}
