package de.msl.mobilesleeplab.communication;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;

import java.util.ArrayList;

import de.msl.mobilesleeplab.plugins.MonitorPlugin;
import de.msl.mobilesleeplab.plugins.ProcessingPlugin;

/**
 * Created by Andreas Burgdorf on 11.02.16.
 * This class described Observers which listen and react to new data from Plugins
 */
public class PluginObserver extends ContentObserver {

    private Context context;

    private ArrayList<String> intValues;
    private ArrayList<String> floatValues;
    private ArrayList<String> stringValues;

    private MonitorPlugin monitorPlugin;
    private ProcessingPlugin processingPlugin;

    private boolean useProcessingPlugin = false;

    /**
     *
     * @param handler Handler which will handle changes, recognized by this Observer
     * @param context Application context
     * @param intValues Number of expected integer values
     * @param floatValues Number of expected float values
     * @param stringValues Number of expected string values
     * @param monitorPlugin MonitorPlugin which integrates this Observer
     */
    public PluginObserver(Handler handler, Context context, ArrayList<String> intValues, ArrayList<String> floatValues, ArrayList<String> stringValues, MonitorPlugin monitorPlugin){

        super(handler);
        this.context = context;
        this.intValues = intValues;
        this.floatValues = floatValues;
        this.stringValues = stringValues;
        this.monitorPlugin = monitorPlugin;
        useProcessingPlugin = false;
    }

    /**
     *
     * @param handler Handler which will handle changes, recognized by this Observer
     * @param context Application context
     * @param intValues Number of expected integer values
     * @param floatValues Number of expected float values
     * @param stringValues Number of expected string values
     * @param processingPlugin ProcessingPlugin which integrates this Observer
     */
    public PluginObserver(Handler handler, Context context, ArrayList<String> intValues, ArrayList<String> floatValues, ArrayList<String> stringValues, ProcessingPlugin processingPlugin){

        super(handler);
        this.context = context;
        this.intValues = intValues;
        this.floatValues = floatValues;
        this.stringValues = stringValues;
        this.processingPlugin = processingPlugin;
        useProcessingPlugin = true;
    }

    @Override
    public void onChange(boolean selfChange){
        this.onChange(selfChange, null);
    }

    /**
     *  This method is called when a change is detected at the respective URI
     * @param selfChange
     * @param uri URI of the detected change
     */
    @Override
    public void onChange(boolean selfChange, Uri uri){

        Cursor c = context.getContentResolver().query(uri, null, null, null, null);

        if (c.moveToFirst()) {
            do {

                ArrayList<Integer> ints = new ArrayList<>();
                ArrayList<Float> floats = new ArrayList<>();
                ArrayList<String> strings = new ArrayList<>();

                String time_from = c.getString(1);
                String time_to = c.getString(2);

                int i = 3;

                if(intValues!=null){
                    for(String meaning : intValues){
                        ints.add(c.getInt(i));
                        i++;
                    }
                }
                if(floatValues!=null){
                    for(String meaning : floatValues){
                        floats.add(c.getFloat(i));
                        i++;
                    }
                }
                if(stringValues!=null){
                    for(String meaning : stringValues){
                        strings.add(c.getString(i));
                        i++;
                    }
                }
                if(!useProcessingPlugin)
                    monitorPlugin.addValue(time_from, time_to, ints, floats, strings, context);
                else
                    processingPlugin.addValue(time_from, time_to, ints, floats, strings, context);
            } while (c.moveToNext());

        }
        c.close();
    }
}
