package de.msl.mobilesleeplab.data;

import java.util.ArrayList;

import de.msl.mobilesleeplab.model.ValueMeaning;

/**
 * Created by Andreas on 28.04.2016.
 * This class represents the data which certain plugins presume for running
 */
public class NeededInfo {

    private int neededNrOfInts;
    private int neededNrOfFloats;
    private int neededNrOfStrings;

    private ArrayList<String> neededIntMeaning;
    private ArrayList<String> neededFloatMeaning;
    private ArrayList<String> neededStringMeaning;

    private ArrayList<ValueMeaning> valueMeanings;

    /**
     *
     * @param neededNrOfInts    the nr of ints, the plugin needs
     * @param neededNrOfFloats  the nr of floats, the plugin needs
     * @param neededNrOfStrings the nr of strings, the plugin needs
     * @param neededIntMeaning  the meaning of the needed ints
     * @param neededFloatMeaning    the meaning of the needed floats
     * @param neededStringMeaning   the meaning of the needed strings
     */
    public NeededInfo(int neededNrOfInts, int neededNrOfFloats, int neededNrOfStrings, ArrayList<String> neededIntMeaning, ArrayList<String> neededFloatMeaning, ArrayList<String> neededStringMeaning){
        this.neededNrOfInts = neededNrOfInts;
        this.neededNrOfFloats = neededNrOfFloats;
        this.neededNrOfStrings = neededNrOfStrings;

        this.neededIntMeaning = neededIntMeaning;
        this.neededFloatMeaning = neededFloatMeaning;
        this.neededStringMeaning = neededStringMeaning;

        valueMeanings = new ArrayList<>();

        if(neededNrOfInts>0){
            for(String intMeaning : neededIntMeaning){
                valueMeanings.add(new ValueMeaning(intMeaning));
            }
        }
        if(neededNrOfFloats>0){
            for(String floatMeaning : neededFloatMeaning){
                valueMeanings.add(new ValueMeaning(floatMeaning));
            }
        }

        if(neededNrOfStrings>0){
            for(String stringMeaning : neededStringMeaning){
                valueMeanings.add(new ValueMeaning(stringMeaning));
            }
        }

    }

    public int getNeededNrOfInts() {
        return neededNrOfInts;
    }

    public int getNeededNrOfFloats() {
        return neededNrOfFloats;
    }

    public int getNeededNrOfStrings() {
        return neededNrOfStrings;
    }

    public ArrayList<String> getNeededIntMeaning() {
        return neededIntMeaning;
    }

    public ArrayList<String> getNeededFloatMeaning() {
        return neededFloatMeaning;
    }

    public ArrayList<String> getNeededStringMeaning() {
        return neededStringMeaning;
    }

    /**
     * Returns the needed values in the better processable ValueMeaning format
     * @return NeededInfo of the plugin as ArrayList of ValueMeanings
     */
    public ArrayList<ValueMeaning> getValueMeanings() {
        return valueMeanings;
    }
}
