package de.msl.mobilesleeplab.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import de.msl.mobilesleeplab.data.CustomSQLHelper;
import de.msl.mobilesleeplab.data.MslContentProvider;
import de.msl.mobilesleeplab.data.PluginDataSource;
import de.msl.mobilesleeplab.data.ProvidedInfo;
import de.msl.mobilesleeplab.plugins.MonitorPlugin;
import de.msl.mobilesleeplab.plugins.Plugin;
import de.msl.mobilesleeplab.plugins.PresentationPlugin;
import de.msl.mobilesleeplab.plugins.ProcessingPlugin;

/**
 * Created by Andreas on 11.02.16.
 * Manager for Plugins
 */
public class PluginManager {

    private static final String TAG = "PluginManager";

    private static PluginManager managerInstance = null;

    private ArrayList<MonitorPlugin> monitorPlugins;
    private ArrayList<ProcessingPlugin> processingPlugins;
    private ArrayList<PresentationPlugin> presentationPlugins;

    private ArrayList<MonitorPlugin> activeMonitorPlugins;
    private ArrayList<ProcessingPlugin> activeProcessingPlugins;

    private ArrayList<ProvidedInfo> providedInfos;

    private CustomSQLHelper customSQLHelper;

    private PluginDataSource pluginDataSource;

    private PluginManager(){
        monitorPlugins = new ArrayList<>();
        processingPlugins = new ArrayList<>();
        presentationPlugins = new ArrayList<>();
        activeMonitorPlugins = new ArrayList<>();
        activeProcessingPlugins = new ArrayList<>();
        providedInfos = new ArrayList<>();
    }

    /**
     * Returns only instance of PluginManager
     * @return instance of plugin manager
     */
    protected static PluginManager getInstance(){
        if(managerInstance == null){
            managerInstance = new PluginManager();
        }
        return managerInstance;
    }

    /**
     * Initialize plugin manager
     * @param context application context
     */
    public void init(Context context){
        prepareDatabase(context);
        pluginDataSource = new PluginDataSource();
    }

    /////////////////////////////////////////////////////////////////////////
    //              Getter and Setter
    /////////////////////////////////////////////////////////////////////////

    public ArrayList<Plugin> getAllPlugins(){
        ArrayList<Plugin> allPlugins = new ArrayList<>();
        if(monitorPlugins != null){
            if (monitorPlugins.size()>0){
                for(MonitorPlugin plugin : monitorPlugins)
                    allPlugins.add(plugin);
            }
        }
        if(processingPlugins != null){
            if (processingPlugins.size()>0){
                for(ProcessingPlugin plugin : processingPlugins)
                    allPlugins.add(plugin);
            }
        }
        if(presentationPlugins != null){
            if (presentationPlugins.size()>0){
                for(PresentationPlugin plugin : presentationPlugins)
                    allPlugins.add(plugin);
            }
        }
        return allPlugins;
    }

    /**
     * Returns a list of the names of active monitor plugins
     * @return list of active monitor plugin names
     */
    public String getActiveMonitorPluginNameList(){
        String res = "";
        if(activeMonitorPlugins.size()>0){
            for(MonitorPlugin plugin : activeMonitorPlugins){
                res += plugin.getName() + "\n";
            }
        }
        return res;
    }

    /**
     * Returns a list of the names of active monitor plugins
     * @return list of active monitor plugin names
     */
    public String getActiveProcessingPluginNameList(){
        String res = "";
        if(activeProcessingPlugins.size()>0){
            for(ProcessingPlugin plugin : activeProcessingPlugins){
                res += plugin.getName() + "\n";
            }
        }
        return res;
    }

    /**
     * Returns the SQL Helper
     * @return Custom SQL helper
     */
    public CustomSQLHelper getCustomSQLHelper(){
        return customSQLHelper;
    }

    /**
     * Returns all MonitorPlugins
     * @return List of all MonitorPlugins
     */
    public ArrayList<MonitorPlugin> getMonitorPlugins(){
        return monitorPlugins;
    }

    /**
     * Returns all ProcessingPlugins
     * @return List of all ProcessingPlugins
     */
    public ArrayList<ProcessingPlugin> getProcessingPlugins(){ return processingPlugins;}

    /**
     * Returns all PresentationPlugins
     * @return List of all PresentationPlugins
     */
    public ArrayList<PresentationPlugin> getPresentationPlugins(){
        return presentationPlugins;
    }

    /**
     * Returns a plugin object based on its name
     * @param name plugin name
     * @return Plugin with given name
     */
    public Plugin getPluginByName(String name){
        for(Plugin p : getAllPlugins()){
            if(p.getName().equals(name)){
                return p;
            }
        }
        return null;
    }

    /**
     * Returns all active MonitorPlugins
     * @return List of all active monitor plugins
     */
    public ArrayList<MonitorPlugin> getActiveMonitorPlugins(){
        ArrayList<MonitorPlugin> plugins = new ArrayList<>();
        for(MonitorPlugin p : getMonitorPlugins()){
            if(p.isActive()){
                plugins.add(p);
            }
        }
        return plugins;
    }

    /**
     * Returns all active MonitorPlugins
     * @return List of all active monitor plugins
     */
    public ArrayList<ProcessingPlugin> getActiveProcessingPlugins(){
        ArrayList<ProcessingPlugin> plugins = new ArrayList<>();
        for(ProcessingPlugin p : getProcessingPlugins()){
            if(p.isActive()){
                plugins.add(p);
            }
        }
        return plugins;
    }

    /////////////////////////////////////////////////////////////////////////
    //              Preparation
    /////////////////////////////////////////////////////////////////////////

    /**
     * Detects and initializes all installed plugins
     * @param context application context
     */
    public void detectAllPlugins(Context context){
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentServices(new Intent("de.msl.MonitorPlugin"),PackageManager.GET_META_DATA);
        if(resolveInfos.size()>0){
            for(ResolveInfo info : resolveInfos){
                try {
                    MonitorPlugin p = new MonitorPlugin(info);
                    addMonitorPlugin(p);
                }
                catch(Exception ex){
                    Log.d(TAG, "Plugin could not be added");
                }
            }
        }
        List<ResolveInfo> evaluationInfos = pm.queryIntentServices(new Intent("de.msl.ProcessingPlugin"), PackageManager.GET_META_DATA);
        if(evaluationInfos.size()>0){
            for(ResolveInfo info : evaluationInfos){
                try{
                    ProcessingPlugin p = new ProcessingPlugin(info);
                    addProcessingPlugin(p);
                }
                catch(Exception e){

                }
            }
        }
        List<ResolveInfo> presentationInfos = pm.queryIntentServices(new Intent("de.msl.PresentationPlugin"), PackageManager.GET_META_DATA);
        if(presentationInfos.size()>0){
            for(ResolveInfo info : presentationInfos){
                try{
                    PresentationPlugin p = new PresentationPlugin(info);
                    addPresentationPlugin(p);
                }
                catch(Exception e){

                }
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////
    //              Activate / Deactivate
    /////////////////////////////////////////////////////////////////////////

    /**
     * Activates a MonitorPlugin based on its name
     * @param name name of the plugin to activate
     */
    public void activateMonitorPlugin(String name){
        if(monitorPlugins.size()>0){
            for(MonitorPlugin monitorPlugin : monitorPlugins){
                if(monitorPlugin.getName().equals(name)){
                    if(monitorPlugin.isActive() || activeMonitorPlugins.contains(monitorPlugin)){
                        return;
                    }

                    activeMonitorPlugins.add(monitorPlugin);
                    monitorPlugin.activatePlugin();
                    MslContentProvider.addPluginUri(monitorPlugin);
                    verifyPluginVersion(monitorPlugin);

                    break;
                }
            }
        }
    }

    /**
     * Activates a ProcessingPlugin based on its name
     * @param name name of the plugin to activate
     */
    public void activateProcessingPlugin(String name){
        if(processingPlugins.size()>0){
            for(ProcessingPlugin processingPlugin : processingPlugins){
                if(processingPlugin.getName().equals(name)){
                    if(processingPlugin.isActive() || activeProcessingPlugins.contains(processingPlugin)){
                        return;
                    }

                    activeProcessingPlugins.add(processingPlugin);
                    processingPlugin.activatePlugin();
                    MslContentProvider.addPluginUri(processingPlugin);
                    verifyPluginVersion(processingPlugin);

                    break;
                }
            }
        }
    }

    /**
     * Deactivates a MonitorPlugin based on its name
     * @param name name of the plugin to deactivate
     * @return true if successful
     */
    public boolean deactivateMonitorPlugin(String name){
        if(activeMonitorPlugins.size()>0){
            for(MonitorPlugin monitorPlugin : activeMonitorPlugins){
                if(monitorPlugin.getName().equals(name)){
                    if(!monitorPlugin.isActive()){
                        return false;
                    }
                    activeMonitorPlugins.remove(monitorPlugin);
                    providedInfos.remove(monitorPlugin.getProvidedInfo());
                    monitorPlugin.deactivatePlugin();
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Deactivates a ProcessingPlugin based on its name
     * @param name name of the plugin to deactivate
     * @return true if successful
     */
    public boolean deactivateProcessingPlugin(String name){
        if(activeProcessingPlugins.size()>0){
            for(ProcessingPlugin processingPlugin : activeProcessingPlugins){
                if(processingPlugin.getName().equals(name)){
                    if(!processingPlugin.isActive()){
                        return false;
                    }
                    activeProcessingPlugins.remove(processingPlugin);
                    providedInfos.remove(processingPlugin.getProvidedInfo());
                    processingPlugin.deactivatePlugin();
                    return true;
                }
            }
        }
        return false;
    }

    /////////////////////////////////////////////////////////////////////////
    //              Database
    /////////////////////////////////////////////////////////////////////////

    /**
     * prepares the database
     * @param context application context
     */
    public void prepareDatabase(Context context){
        customSQLHelper = new CustomSQLHelper(context, "MOBILESLEEPLAB", 6, providedInfos);
    }

    /////////////////////////////////////////////////////////////////////////
    //              Helper
    /////////////////////////////////////////////////////////////////////////

    /**
     * Adds a new MonitorPlugin
     * @param monitorPlugin MonitorPlugin to add
     */
    public void addMonitorPlugin(MonitorPlugin monitorPlugin){
        if(!monitorPluginExisting(monitorPlugin))
            monitorPlugins.add(monitorPlugin);
    }

    /**
     * Adds a new ProcessingPlugin
     * @param processingPlugin ProcessingPlugin to add
     */
    public void addProcessingPlugin(ProcessingPlugin processingPlugin){
        if(!processingPluginExisting(processingPlugin)){
            processingPlugins.add(processingPlugin);
        }
    }

    /**
     * Adds a new PresentationPlugin
     * @param presentationPlugin PresentationPlugin to add
     */
    public void addPresentationPlugin(PresentationPlugin presentationPlugin){
        if(!presentationPluginExisting(presentationPlugin)){
            presentationPlugins.add(presentationPlugin);
        }
    }

    /**
     * Adds privded infos to the list of provided infos
     * @param providedInfo providedInfo to add
     */
    public void addDataInfo(ProvidedInfo providedInfo){
        if(!providedInfos.contains(providedInfo)){
            providedInfos.add(providedInfo);
        }
    }

    /**
     * Checks if a MonitorPlugin is existing
     * @param monitorPlugin MonitorPlugin to check
     * @return true if existing
     */
    public boolean monitorPluginExisting(MonitorPlugin monitorPlugin){
        if(monitorPlugins.contains(monitorPlugin)){
            return true;
        }
        return false;
    }

    /**
     * Checks if a ProcessingPlugin is existing
     * @param processingPlugin ProcessingPlugin to check
     * @return true if existing
     */
    public boolean processingPluginExisting(ProcessingPlugin processingPlugin){
        if(processingPlugins.contains(processingPlugin)){
            return true;
        }
        return false;
    }

    /**
     * Checks if a PresentationPlugin is existing
     * @param presentationPlugin PresentationPlugin to check
     * @return true if existing
     */
    public boolean presentationPluginExisting(PresentationPlugin presentationPlugin){
        if(presentationPlugins.contains(presentationPlugin)){
            return true;
        }
        return false;
    }

    /**
     * Creates a database table from the ProvidedInfo of a Plugin
     * @param providedInfo ProvidedInfo of a Plugin
     */
    public void createTable(ProvidedInfo providedInfo){
        SQLiteDatabase database = customSQLHelper.getWritableDatabase();
        boolean existing = false;
        String[] res = {"name"};
        try{

            Cursor cursor  = database.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"+ providedInfo.getTABLENAME()+"'", null);
            cursor.moveToFirst();
            String result = cursor.getString(0);

            if(result.equals(providedInfo.getTABLENAME())){
                existing = true;
            }

        }
        catch (Exception e){
            database.execSQL(providedInfo.getCreateCommand());
        }
    }

    /**
     * Loads plugins from the settings
     * @param preferences System preferences
     */
    public void loadPluginsFromSettings(SharedPreferences preferences){
        if(getMonitorPlugins().size()>0) {
            for (Plugin plugin : getMonitorPlugins()) {
                if (preferences.getBoolean(plugin.getName(), false)) {
                    activateMonitorPlugin(plugin.getName());
                }
            }
            for(Plugin plugin : getProcessingPlugins()){
                if(preferences.getBoolean(plugin.getName(), false)){
                    activateProcessingPlugin(plugin.getName());
                }
            }
        }
    }

    /**
     * Verifies the version of a MonitorPlugin
     * @param monitorPlugin MonitorPlugin to check
     */
    private void verifyPluginVersion(MonitorPlugin monitorPlugin){
        if(pluginDataSource.getOldVersion(monitorPlugin)==0) {
            createTable(monitorPlugin.getProvidedInfo());
            pluginDataSource.registerPlugin(monitorPlugin);
        }
        else{
            if(pluginDataSource.getOldVersion(monitorPlugin)< monitorPlugin.getVersion()){
                pluginDataSource.dropTable(monitorPlugin);
                createTable(monitorPlugin.getProvidedInfo());
                pluginDataSource.updateRecord(monitorPlugin);
            }
        }
    }

    /**
     * Verifies the version of a MonitorPlugin
     * @param processingPlugin ProcessingPlugin to check
     */
    private void verifyPluginVersion(ProcessingPlugin processingPlugin){
        if(pluginDataSource.getOldVersion(processingPlugin)==0) {
            createTable(processingPlugin.getProvidedInfo());
            pluginDataSource.registerPlugin(processingPlugin);
        }
        else{
            if(pluginDataSource.getOldVersion(processingPlugin)< processingPlugin.getVersion()){
                pluginDataSource.dropTable(processingPlugin);
                createTable(processingPlugin.getProvidedInfo());
                pluginDataSource.updateRecord(processingPlugin);
            }
        }
    }

}
