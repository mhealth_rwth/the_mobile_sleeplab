package de.msl.mobilesleeplab.utils;

import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

import de.msl.mobilesleeplab.data.NeededInfo;
import de.msl.mobilesleeplab.data.ProvidedInfo;
import de.msl.mobilesleeplab.data.RecordDataSource;
import de.msl.mobilesleeplab.model.Record;
import de.msl.mobilesleeplab.model.ValueMeaning;
import de.msl.mobilesleeplab.plugins.MonitorPlugin;
import de.msl.mobilesleeplab.plugins.PresentationPlugin;
import de.msl.mobilesleeplab.plugins.ProcessingPlugin;

/**
 * Created by Andreas on 27.04.2016.
 * Manager for Records
 */
public class RecordManager {

    private static RecordManager managerInstance = null;

    private RecordDataSource recordDataSource;

    private Record currentRecord;

    public static RecordManager getInstance(){
        if(managerInstance == null){
            managerInstance = new RecordManager();
        }
        return managerInstance;
    }

    /**
     * Initialize RecordManager
     */
    public void init(){
        setRecordDataSource(new RecordDataSource());
    }

    /**
     * Returns the RecordDataSource
     * @return RecordDataSource
     */
    public RecordDataSource getRecordDataSource(){
        if(recordDataSource == null){
            recordDataSource = new RecordDataSource();
        }
        return recordDataSource;
    }

    /**
     * Sets the RecordDataSource
     * @param recordDataSource RecordDataSource to use
     */
    public void setRecordDataSource(RecordDataSource recordDataSource){
        this.recordDataSource = recordDataSource;
    }

    /**
     * Returns the currently ongoing record
     * @return Currently active Record
     */
    public Record getCurrentRecord() {
        if (currentRecord != null){
            return currentRecord;
        }
        return  getRecordDataSource().getLastRecord();
    }

    /**
     * Sets the currently ongoing record
     * @param currentRecord Currently active Record
     */
    public void setCurrentRecord(Record currentRecord) {
        this.currentRecord = currentRecord;
    }

    /**
     * Prepares a new Record
     * @param description Description of the Record
     * @return stored Record
     */
    public Record prepareNewRecord(String description){
        setCurrentRecord(new Record());
        currentRecord.setDescription(description);
        currentRecord.startRecord();
        currentRecord.setUser(SleepManager.getInstance().getUserManager().getCurrentUser());

        getRecordDataSource().createRecord(currentRecord);

        return currentRecord;
    }

    /**
     * Finish the ongoing Record.
     */
    public void finishRecord(){
        getCurrentRecord().finishRecord();
        getRecordDataSource().updateRecord(currentRecord);
    }

    /**
     * Returns a record based on its id
     * @param id id to fetch
     * @return Record with respective id
     */
    public Record getRecord(long id){
        return getRecordDataSource().getRecordById(id);
    }

    /**
     * Returns all stored Records
     * @return List of all Records.
     */
    public List<Record> getAllRecords(){
        return getRecordDataSource().getAllRecords();
    }

    /**
     * Repairs all records for the case of complications
     */
    public void repairAllRecords(){
        List<Record> allRecords = recordDataSource.getAllRecords();
        if(allRecords.size()>0){
            for(Record record : allRecords){
                if(record.getDateTo().equals("")){
                    record.repairEndTime();
                    getRecordDataSource().updateRecord(record);
                }
            }
        }
    }

    /**
     * Stores all plugins which were used for a Record to a given Intent
     * @param record Record to look for
     * @param intent Intent to store results into
     */
    public void getPluginsForRecord(Record record, Intent intent){
        ArrayList<String> allPlugins = new ArrayList<>();
        for (MonitorPlugin p : record.getUsedMonitorPlugins()) {
                allPlugins.add(p.getName());
                intent.putStringArrayListExtra(p.getName(), p.getAllDataColums());
        }
        intent.putStringArrayListExtra("allPlugins", allPlugins);
    }

    /**
     * Stores all plugins which were used for a Record to a given Intent
     * @param record Record to look for
     * @param intent Intent to store results into
     */
    public void getPluginsForRecordWithProcessing(Record record, Intent intent){
        ArrayList<String> allPlugins = new ArrayList<>();
        for (MonitorPlugin p : record.getUsedMonitorPlugins()) {
            allPlugins.add(p.getName());
            intent.putStringArrayListExtra(p.getName(), p.getAllDataColums());
        }
        for (ProcessingPlugin p : SleepManager.getInstance().getPluginManager().getActiveProcessingPlugins()){
            allPlugins.add(p.getName());
            intent.putStringArrayListExtra(p.getName(), p.getAllDataColums());
        }
        intent.putStringArrayListExtra("allPlugins", allPlugins);
    }

    /**
     * Checks if a record fullfills the requirements for a PresentationPlugin
     * @param record Record to check
     * @param presentationPlugin PresentationPlugin to check
     * @return
     */
    public boolean recordFitsForPresentation(Record record, PresentationPlugin presentationPlugin){
        ArrayList<String> neededValues = new ArrayList<>();
        ArrayList<ValueMeaning> neededValuesExtended = presentationPlugin.getNeededInfo().getValueMeanings();
        ArrayList<String> availableValues = new ArrayList<>();
        ArrayList<ValueMeaning> availableValuesExtended = new ArrayList<>();
        NeededInfo neededInfo = presentationPlugin.getNeededInfo();
        if(neededInfo.getNeededNrOfInts()>0){
            for(String s : neededInfo.getNeededIntMeaning()){
                neededValues.add(s);
            }
        }
        if(neededInfo.getNeededNrOfFloats()>0){
            for(String s : neededInfo.getNeededFloatMeaning()){
                neededValues.add(s);
            }
        }
        if(neededInfo.getNeededNrOfStrings()>0){
            for(String s : neededInfo.getNeededStringMeaning()){
                neededValues.add(s);
            }
        }

        ArrayList<MonitorPlugin> allPlugins = record.getUsedMonitorPlugins();
        for(MonitorPlugin p : allPlugins){
            ProvidedInfo d = p.getProvidedInfo();
            availableValuesExtended.addAll(d.getValueMeanings());
            ArrayList<String> values = d.getCOLUMNS();
            for(String v : values){
                if(neededValues.contains(v)){
                    availableValues.add(v);
                }
            }
        }
        if(neededValues.size() == availableValues.size()){
            return true;
        }

        boolean allOk = true;

        for(ValueMeaning mv : neededValuesExtended){
            boolean currentOk = false;
            for(ValueMeaning mva : availableValuesExtended){
                if((mv.getKindOfValue().equals(mva.getKindOfValue()) && (!mv.isLocationIsRelevant() || (mv.getLocation().equals(mva.getLocation()))))){
                    if(mv.isHasExtension()){
                        if(mv.getValueExtension().equals(mva.getValueExtension())){
                            currentOk = true;
                        }
                    }else {
                        currentOk = true;
                    }
                }
            }
            if(!currentOk){
                allOk = false;
            }
        }

        return allOk;

    }

    /**
     * Checks if a record fullfills the requirements for a ProcessingPlugin
     * @param record Record to check
     * @param processingPlugin ProcessingPlugin to check
     * @return
     */
    public boolean recordFitsForProcessing(Record record, ProcessingPlugin processingPlugin){
        ArrayList<String> neededValues = new ArrayList<>();
        ArrayList<ValueMeaning> neededValuesExtended = processingPlugin.getNeededInfo().getValueMeanings();
        ArrayList<String> availableValues = new ArrayList<>();
        ArrayList<ValueMeaning> availableValuesExtended = new ArrayList<>();
        NeededInfo neededInfo = processingPlugin.getNeededInfo();
        if(neededInfo.getNeededNrOfInts()>0){
            for(String s : neededInfo.getNeededIntMeaning()){
                neededValues.add(s);
            }
        }
        if(neededInfo.getNeededNrOfFloats()>0){
            for(String s : neededInfo.getNeededFloatMeaning()){
                neededValues.add(s);
            }
        }
        if(neededInfo.getNeededNrOfStrings()>0){
            for(String s : neededInfo.getNeededStringMeaning()){
                neededValues.add(s);
            }
        }

        ArrayList<MonitorPlugin> allPlugins = record.getUsedMonitorPlugins();
        for(MonitorPlugin p : allPlugins){
            ProvidedInfo d = p.getProvidedInfo();
            availableValuesExtended.addAll(d.getValueMeanings());
            ArrayList<String> values = d.getCOLUMNS();
            for(String v : values){
                if(neededValues.contains(v)){
                    availableValues.add(v);
                }
            }
        }
        if(neededValues.size() == availableValues.size()){
            return true;
        }

        boolean allOk = true;

        for(ValueMeaning mv : neededValuesExtended){
            boolean currentOk = false;
            for(ValueMeaning mva : availableValuesExtended){
                if((mv.getKindOfValue().equals(mva.getKindOfValue()) && (!mv.isLocationIsRelevant() || (mv.getLocation().equals(mva.getLocation()))))){
                    if(mv.isHasExtension()){
                        if(mv.getValueExtension().equals(mva.getValueExtension())){
                            currentOk = true;
                        }
                    }else {
                        currentOk = true;
                    }
                }
            }
            if(!currentOk){
                allOk = false;
            }
        }

        return allOk;

    }
}
