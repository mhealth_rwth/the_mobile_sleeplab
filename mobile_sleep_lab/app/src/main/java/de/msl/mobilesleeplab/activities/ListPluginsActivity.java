package de.msl.mobilesleeplab.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.msl.mobilesleeplab.R;
import de.msl.mobilesleeplab.model.Observation;
import de.msl.mobilesleeplab.plugins.MonitorPlugin;
import de.msl.mobilesleeplab.utils.SleepManager;

/**
 * This activity allows to see all available monitor plugins and to activate or deactive each
 */
public class ListPluginsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_plugins);

        addAllControls();

        Button buttonStartAll = findViewById(R.id.buttonStartAll);
        Button buttonStopAll = findViewById(R.id.buttonStopAll);

        buttonStartAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SleepManager.getInstance().getMeasurementManager().startNewMeasurement(ListPluginsActivity.this, "");
            }
        });

        buttonStopAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SleepManager.getInstance().getMeasurementManager().stopMeasurement(ListPluginsActivity.this);
            }
        });

    }

    /**
     * Creates a list and checkboxes from all found plugins
     */
    public void addAllControls(){
        ArrayList<MonitorPlugin> monitorPlugins = SleepManager.getInstance().getPluginManager().getMonitorPlugins();

        for(MonitorPlugin monitorPlugin : monitorPlugins){
            Button b1 = new Button(this);
            Button b2 = new Button(this);
            final TextView counter = new TextView(this);
            final TextView title = new TextView(this);
            final MonitorPlugin p = monitorPlugin;
            b1.setText("Activate ");
            b2.setText("Deactivate ");
            counter.setText("Entries");
            title.setText(p.getName());
            b1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SleepManager.getInstance().getPluginManager().activateMonitorPlugin(p.getName());
                }
            });

            b2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SleepManager.getInstance().getPluginManager().deactivateMonitorPlugin(p.getName());
                    List<Observation> obs = p.getAllValues();

                    String text1 = Integer.toString(obs.size());
                    counter.setText(text1);
                }
            });

            LinearLayout innerLayout = new LinearLayout(this);
            innerLayout.setOrientation(LinearLayout.HORIZONTAL);

            innerLayout.addView(b1);
            innerLayout.addView(b2);
            innerLayout.addView(counter);

            LinearLayout layout = findViewById(R.id.linlayout);
            layout.addView(title);
            layout.addView(innerLayout);
        }
    }
}
