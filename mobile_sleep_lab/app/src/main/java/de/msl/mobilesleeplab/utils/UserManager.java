package de.msl.mobilesleeplab.utils;

import java.util.List;

import de.msl.mobilesleeplab.data.UserDataSource;
import de.msl.mobilesleeplab.model.User;

/**
 * Created by Andreas on 23.08.2016.
 * User Manager
 */
public class UserManager {

    private static UserManager managerInstance = null;

    private UserDataSource userDataSource;

    private User currentUser;

    /**
     * Returns only instance of UserManager
     * @return UserManager instance
     */
    protected static UserManager getInstance(){
        if(managerInstance == null){
            managerInstance = new UserManager();
        }
        return managerInstance;
    }

    /**
     * Initialize UserManager
     */
    public void init(){
        setUserDataSource(new UserDataSource());
        try {
            setCurrentUser(userDataSource.getAllUsers().iterator().next());
        }
        catch (Exception e){
            User u = new User("John","Doe");
            addNewUser(u);

            setCurrentUser(u);
        }
    }

    /**
     * Returns the UserDataSource
     * @return UserDataSource
     */
    public UserDataSource getUserDataSource(){
        if(userDataSource == null){
            userDataSource = new UserDataSource();
        }
        return userDataSource;
    }

    /**
     * Sets the UserDataSource
     * @param userDataSource UserDataSource
     */
    public void setUserDataSource(UserDataSource userDataSource){
        this.userDataSource = userDataSource;
    }

    /**
     * Sets the current User
     * @param currentUser User to set
     */
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    /**
     * Returns the current User
     * @return current User
     */
    public User getCurrentUser(){
        return currentUser;
    }

    /**
     * Registers a new User to the database
     * @param user User to register
     * @return currentUser
     */
    public User addNewUser(User user){

        getUserDataSource().registerUser(user);
        setCurrentUser(user);

        return currentUser;
    }

    /**
     * Returns user based on his id
     * @param id user id
     * @return User with respective id
     */
    public User getUserById(int id){
        return getUserDataSource().getUserById(id);
    }

    /**
     * Returns all registered Users
     * @return List of all Users
     */
    public List<User> getAllUsers(){
        return getUserDataSource().getAllUsers();
    }
}
