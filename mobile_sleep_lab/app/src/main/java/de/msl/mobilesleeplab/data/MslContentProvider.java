package de.msl.mobilesleeplab.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;

import de.msl.mobilesleeplab.plugins.MonitorPlugin;
import de.msl.mobilesleeplab.plugins.Plugin;
import de.msl.mobilesleeplab.utils.SleepManager;

/**
 * Created by Andreas on 11.02.16.
 * Inheritance of a ContentProvider which provides the data of all plugins, stored in the core application
 */
public class MslContentProvider extends ContentProvider {

    private static ArrayList<MonitorPlugin> monitorPlugins;

    public final static String AUTHORITY = "de.msl.cpmobilesleeplab";

    private static ArrayList<String> basepaths;

    private static ArrayList<Uri> contentUris;

    private static ArrayList<Integer> idValues;
    private static ArrayList<Integer> idValue_ids;

    private static SQLiteDatabase database;

    private static int ctr = 0;

    public static void addBasePath(String path){
        basepaths.add(path);
        contentUris.add(Uri.parse("content://" + AUTHORITY + "/" + path));
    }

    private  static UriMatcher uriMatcher;
    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        if((basepaths != null )&&(basepaths.size()>0)) {
            for (String basepath : basepaths) {
                uriMatcher.addURI(AUTHORITY, basepath, idValues.get(basepaths.indexOf(basepath)));
                uriMatcher.addURI(AUTHORITY, basepath + "/#", idValues.get(basepaths.indexOf(basepath)));
            }
        }
    }

    public static void init(){
        database = SleepManager.getInstance().getPluginManager().getCustomSQLHelper().getWritableDatabase();
    }

    /**
     * Adds a new Plugin to the Content Provider
     * @param plugin Plugin to be added
     */
    public static void addPluginUri(Plugin plugin){
        if(!basepaths.contains(plugin.getName())){
            basepaths.add(plugin.getName());
            contentUris.add(Uri.parse("content://" + AUTHORITY + "/" + plugin.getName()));

            int value1 = ++ ctr;
            int value2 = ++ ctr;

            idValues.add(value1);
            idValue_ids.add(value2);

            uriMatcher.addURI(AUTHORITY, plugin.getName(), value1);
            uriMatcher.addURI(AUTHORITY, plugin.getName() + "/#", value2);
        }
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
        try {
            int id = uriMatcher.match(uri);
            return "vnd.android.cursor.dir/vnd." + AUTHORITY + "/" + basepaths.get(id);
        }
        catch (Exception ex){
            throw new IllegalArgumentException("Unsupported URI: " + uri.toString());
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Uri destinationUri = null;

        int id = uriMatcher.match(uri);
        int index = idValues.indexOf(id);
        if(index==-1){
            index = idValue_ids.indexOf(id);
        }
        long insertId = 0;
        database = SleepManager.getInstance().getPluginManager().getCustomSQLHelper().getWritableDatabase();
        insertId = database.insert(basepaths.get(index), "", values);
        if(insertId>-1) {
            Uri result = ContentUris.withAppendedId(Uri.parse("content://de.msl.cpmobilesleeplab/" + basepaths.get(index) + "/"), insertId);
            getContext().getContentResolver().notifyChange(result,null);
            return result;
        }

        return destinationUri;
    }

    @Override
    public boolean onCreate() {
        monitorPlugins = SleepManager.getInstance().getPluginManager().getMonitorPlugins();
        basepaths = new ArrayList<>();
        contentUris = new ArrayList<>();
        idValue_ids = new ArrayList<>();
        idValues = new ArrayList<>();
        ctr = 0;

        //database = PluginManager.getInstance().getCustomSQLHelper().getWritableDatabase();

        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        SQLiteDatabase db = database;
        SQLiteQueryBuilder sqlBuilder = new SQLiteQueryBuilder();

        int id = uriMatcher.match(uri);

        int index = idValues.indexOf(id);
        if(index==-1){
            index = idValue_ids.indexOf(id);
        }

        //case even : many
        sqlBuilder.setTables(basepaths.get(index));


        if((id % 2) != 0){
            Log.i("test", "send all");
        }

        //case odd : one
        else{
            sqlBuilder.appendWhere("record = " + uri.getLastPathSegment());
        }

        boolean stat = db.isOpen();
        db = SleepManager.getInstance().getPluginManager().getCustomSQLHelper().getWritableDatabase();
        stat = db.isOpen();
        Cursor cursor = sqlBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;

    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
