package de.msl.mobilesleeplab.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Andreas on 11.02.16.
 * Helper class for the different data sources of the mobile sleeplab
 */
public class CustomSQLHelper extends SQLiteOpenHelper {

    public ArrayList<ProvidedInfo> providedInfos;

    public String DATABASE_NAME;

    public int DATABASE_VERSION;

    private final String CREATE_TABLE_RECORDS = "create table records(_id integer not null, date_from string not null, date_to string not null, description string not null, plugins string not null, user integer);";

    private final String CREATE_TABLE_USERS = "create table users(_id integer primary key autoincrement, firstname string not null, lastname string not null);";

    private final String CREATE_TABLE_PLUGINS = "create table plugins(_id integer primary key autoincrement, name string not null, version integer not null);";

    public CustomSQLHelper(Context context, String DATABASE_NAME, int DATABASE_VERSION, ArrayList<ProvidedInfo> providedInfos){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        this.DATABASE_NAME = DATABASE_NAME;
        this.DATABASE_VERSION = DATABASE_VERSION;
        this.providedInfos = providedInfos;

    }

    @Override
    public void onCreate(SQLiteDatabase database){

        if(providedInfos != null && providedInfos.size()>0){
            for(ProvidedInfo providedInfo : providedInfos){
                database.execSQL(providedInfo.getCreateCommand());
            }
        }

        database.execSQL(CREATE_TABLE_RECORDS);
        database.execSQL(CREATE_TABLE_USERS);
        database.execSQL(CREATE_TABLE_PLUGINS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(CustomSQLHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");

        if(providedInfos != null && providedInfos.size()>0){
            for(ProvidedInfo providedInfo : providedInfos) {
                db.execSQL("DROP TABLE IF EXISTS " + providedInfo.getTABLENAME());
            }
        }

        db.execSQL("DROP TABLE IF EXISTS records");
        db.execSQL("DROP TABLE IF EXISTS users");
        db.execSQL("DROP TABLE IF EXISTS plugins");
        onCreate(db);
    }

}
