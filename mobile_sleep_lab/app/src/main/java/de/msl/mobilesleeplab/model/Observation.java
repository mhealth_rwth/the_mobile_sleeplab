package de.msl.mobilesleeplab.model;

import java.util.ArrayList;

/**
 * Created by Andreas on 11.02.2016.
 * Wrapped representation of integers, floats and strings, received from a plugin
 */
public class Observation {


    private ArrayList<Integer> ints;
    private ArrayList<Float> floats;
    private ArrayList<String> strings;

    public Observation(ArrayList<Integer> ints, ArrayList<Float> floats, ArrayList<String> strings){
        this.ints = ints;
        this.floats = floats;
        this.strings = strings;
    }

    public ArrayList<Integer> getInts() {
        return ints;
    }

    public void setInts(ArrayList<Integer> ints) {
        this.ints = ints;
    }

    public ArrayList<Float> getFloats() {
        return floats;
    }

    public void setFloats(ArrayList<Float> floats) {
        this.floats = floats;
    }

    public ArrayList<String> getStrings() {
        return strings;
    }

    public void setStrings(ArrayList<String> strings) {
        this.strings = strings;
    }

    @Override
    public String toString(){
        String observ = "Observation: ";
        if(ints != null){
            if(ints.size()>0){
                for(int val : ints){
                    observ += val + " - ";
                }
            }
        }
        if(floats != null){
            if(floats.size()>0){
                for(float val : floats){
                    observ += val + " - ";
                }
            }
        }
        if(strings != null){
            if(strings.size()>0){
                for(String val : strings){
                    observ += val + " - ";
                }
            }
        }
        return observ;
    }


}
