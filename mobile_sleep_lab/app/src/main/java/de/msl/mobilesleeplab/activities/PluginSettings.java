package de.msl.mobilesleeplab.activities;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.util.Log;

import de.msl.mobilesleeplab.R;
import de.msl.mobilesleeplab.model.ValueMeaning;
import de.msl.mobilesleeplab.plugins.MonitorPlugin;
import de.msl.mobilesleeplab.plugins.ProcessingPlugin;
import de.msl.mobilesleeplab.utils.SleepManager;

/**
 * Settings activity for activating or deactivating monitor plugins
 */
public class PluginSettings extends PreferenceActivity {

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        PreferenceScreen preferenceScreen = getPreferenceManager().createPreferenceScreen(this);

        this.getListView().setBackgroundResource(R.drawable.background);

        final SharedPreferences preferences = getSharedPreferences("MSLPREF", MODE_PRIVATE);
        preferences.edit();



        PreferenceCategory category = new PreferenceCategory(this);

        category.setTitle("Select Plugins");
        preferenceScreen.addPreference(category);

        // Settings for monitor plugins
        SleepManager.getInstance();
        if(SleepManager.getPluginManager().getMonitorPlugins() != null) {
            // create checkbox for every monitor plugin
            SleepManager.getInstance();
            for (final MonitorPlugin plugin : SleepManager.getPluginManager().getMonitorPlugins()) {

                CheckBoxPreference checkBoxPreference = new CheckBoxPreference(this);
                checkBoxPreference.setTitle(plugin.getName());
                StringBuilder s = new StringBuilder();
                for(ValueMeaning opt : plugin.getProvidedInfo().getValueMeanings()){
                    s.append(opt.getKindOfValueDescription()).append(" ").append(opt.getLocationDescription()).append("\n");
                }
                checkBoxPreference.setSummary(s.toString());
                if(preferences.getBoolean(plugin.getName(), false)) {
                    checkBoxPreference.setChecked(true);
                }
                else{
                    checkBoxPreference.setChecked(false);
                }
                checkBoxPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        boolean active = (Boolean) newValue;
                        SharedPreferences sp = getSharedPreferences("MSLPREF", MODE_PRIVATE);
                        SharedPreferences.Editor ed = sp.edit();
                        ed.putBoolean(plugin.getName(), (Boolean) newValue);
                        ed.apply();

                        if(active){
                            SleepManager.getInstance();
                            SleepManager.getPluginManager().activateMonitorPlugin(plugin.getName());
                            Log.d("PREFS", "activating " + plugin.getName());
                        }
                        else{
                            SleepManager.getInstance();
                            SleepManager.getPluginManager().deactivateMonitorPlugin(plugin.getName());
                            Log.d("PREFS", "deactivating " + plugin.getName());
                        }

                        return true;
                    }
                });
                category.addPreference(checkBoxPreference);

            }
        }
        // Settings for Processing plugins
        SleepManager.getInstance();
        if(SleepManager.getPluginManager().getProcessingPlugins() != null) {
            // Create checkbox for every processing plugin
            SleepManager.getInstance();
            for (final ProcessingPlugin plugin : SleepManager.getPluginManager().getProcessingPlugins()) {

                CheckBoxPreference checkBoxPreference = new CheckBoxPreference(this);
                checkBoxPreference.setTitle(plugin.getName());
                StringBuilder s = new StringBuilder();
                for(ValueMeaning opt : plugin.getProvidedInfo().getValueMeanings()){
                    s.append(opt.getKindOfValueDescription()).append(" ").append(opt.getLocationDescription()).append("\n");
                }
                checkBoxPreference.setSummary(s.toString());
                if(preferences.getBoolean(plugin.getName(), false)) {
                    checkBoxPreference.setChecked(true);
                }
                else{
                    checkBoxPreference.setChecked(false);
                }
                checkBoxPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        boolean active = (Boolean) newValue;
                        SharedPreferences sp = getSharedPreferences("MSLPREF", MODE_PRIVATE);
                        SharedPreferences.Editor ed = sp.edit();
                        ed.putBoolean(plugin.getName(), (Boolean) newValue);
                        ed.apply();

                        if(active){
                            SleepManager.getInstance();
                            SleepManager.getPluginManager().activateProcessingPlugin(plugin.getName());
                            Log.d("PREFS", "activating " + plugin.getName());
                        }
                        else{
                            SleepManager.getPluginManager().deactivateProcessingPlugin(plugin.getName());
                            Log.d("PREFS", "deactivating " + plugin.getName());
                        }

                        return true;
                    }
                });
                category.addPreference(checkBoxPreference);

            }
        }

        setPreferenceScreen(preferenceScreen);
    }
}
