package de.msl.mobilesleeplab.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.List;

import de.msl.mobilesleeplab.model.Record;
import de.msl.mobilesleeplab.utils.SleepManager;

/**
 * This activity is used to list all existing records
 */
public class ListRecordsActivity extends ListActivity {

    private RecordListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new RecordListAdapter(this, generateData());
        setListAdapter(adapter);
    }

    /**
     * Repairs broken records that have damages due to application crashes
     * @return List of all records
     */
    private List<Record> generateData(){

        SleepManager.getInstance().getRecordManager().repairAllRecords();
        //ArrayList<Record> records = new ArrayList<>();
        //RecordDataSource r = new RecordDataSource();
        //return r.getAllRecords();
        return SleepManager.getInstance().getRecordManager().getAllRecords();
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        long recordid = adapter.getItemsArrayList().get(position).getId();
        Intent intent = new Intent(this, RecordActivity.class);
        intent.putExtra("record_id", recordid);
        startActivity(intent);
    }
}
