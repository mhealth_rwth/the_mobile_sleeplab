package de.msl.mobilesleeplab.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.msl.mobilesleeplab.model.User;
import de.msl.mobilesleeplab.utils.SleepManager;

/**
 * Created by Andreas on 23.08.2016.
 * Datasource for a table which stores information about Users
 */
public class UserDataSource {
    private SQLiteDatabase database;
    private CustomSQLHelper databaseHelper;

    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String TABLENAME = "users";

    public CustomSQLHelper getDbHelper() {
        return databaseHelper;
    }

    public void setDbHelper(CustomSQLHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public UserDataSource(){
        databaseHelper = SleepManager.getInstance().getPluginManager().getCustomSQLHelper();
    }

    public void open() throws SQLException {
        database = databaseHelper.getWritableDatabase();
    }

    public void close(){

    }

    /**
     * Register a new user to the database
     * @param user User to register
     */
    public void registerUser(User user){
        try{
            open();
            String command = "insert into users(firstname, lastname) values ('";
            command += user.getFirstname() + "', '";
            command += user.getLastname() + "');";

            database.execSQL(command);
            close();
        }
        catch (Exception e){

        }
    }

    /**
     * Get all users which are registered in the database
     * @return list of all users
     */
    public List<User> getAllUsers(){
        List<User> result = new ArrayList<>();
        String orderBy =  LASTNAME + " ASC";
        database = SleepManager.getInstance().getPluginManager().getCustomSQLHelper().getWritableDatabase();
        Cursor cursor = database.query(TABLENAME, new String[]{"_id", "firstname", "lastname"}, null, null, null, null, orderBy);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            User user = cursorToUser(cursor);
            result.add(user);
            cursor.moveToNext();
        }
        return result;
    }

    /**
     * Translates a cursor to a user
     * @param cursor cursor as returned from database
     * @return User object
     */
    public User cursorToUser(Cursor cursor){
        User user = new User(cursor.getLong(0), cursor.getString(1), cursor.getString(2));
        return user;
    }

    /**
     * Returns a user with a certain id
     * @param id id of user to be returned
     * @return user
     */
    public User getUserById(int id){
        database = SleepManager.getInstance().getPluginManager().getCustomSQLHelper().getWritableDatabase();
        String selection = "_id = " + id;
        Cursor cursor = database.query(TABLENAME, new String[]{"_id", "firstname", "lastname"}, selection, null, null, null, null);
        cursor.moveToFirst();
        User result = cursorToUser(cursor);
        return result;
    }
}
