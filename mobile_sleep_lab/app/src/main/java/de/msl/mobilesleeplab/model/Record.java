package de.msl.mobilesleeplab.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.msl.mobilesleeplab.plugins.MonitorPlugin;
import de.msl.mobilesleeplab.utils.SleepManager;

/**
 * Created by Andreas on 04.02.16.
 * This class represents a Record: A sequence of measurements during one night.
 */
public class Record {

    private long id;
    private String date_from;
    private String date_to;
    private String description;
    private ArrayList<MonitorPlugin> usedMonitorPlugins;
    private User user;

    public Record(){
        this.date_from = "";
        this.date_to = "";
        this.description = "";
        this.id = 0;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDateFrom() {
        return date_from;
    }

    public void setDateFrom(String date) {
        this.date_from = date;
    }

    public String getDateTo() {
        return date_to;
    }

    public void setDateTo(String date) {
        this.date_to = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUser(User user){
        this.user = user;
    }

    public User getUser(){
        return this.user;
    }

    /**
     * Sets the monitor plugins which are used for this record
     * @param usedMonitorPlugins list of used monitor plugins
     */
    public void setUsedMonitorPlugins(ArrayList<MonitorPlugin> usedMonitorPlugins){
        this.usedMonitorPlugins = usedMonitorPlugins;
    }

    /**
     * Sets the monitor plugins which are used for this record
     * @param usedMonitorPlugins String sequence of names of the usedPlugins
     */
    public void setUsedMonitorPlugins(String usedMonitorPlugins){
        ArrayList<MonitorPlugin> allPlugins = new ArrayList<>();
        String plugins[] = usedMonitorPlugins.split(";");

        for(String s : plugins){
            MonitorPlugin p = (MonitorPlugin) SleepManager.getInstance().getPluginManager().getPluginByName(s);
            allPlugins.add(p);
        }

        this.usedMonitorPlugins = allPlugins;
    }

    /**
     * Returns the List of Monitor plugins which were used for this record
     * @return list of used monitor plugins
     */
    public ArrayList<MonitorPlugin> getUsedMonitorPlugins(){
        return usedMonitorPlugins;
    }

    /**
     * Starts a new record
     */
    public void startRecord(){
        this.date_from = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        this.id = System.currentTimeMillis();
        this.setUsedMonitorPlugins(SleepManager.getInstance().getPluginManager().getActiveMonitorPlugins());
    }

    /**
     * finishs the record
     */
    public void finishRecord(){
        this.date_to = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
    }

    /**
     * Returns used plugins as a list in one string
     * @return string of used records
     */
    public String pluginsToString(){
        String result = "";
        for(MonitorPlugin p : usedMonitorPlugins){
            result += p.getName() + ";";
        }
        result = result.substring(0, result.length()-1);
        return result;
    }

    /**
     * Repairs the end time of the record in the case of crashes during the record
     */
    public void repairEndTime(){
        long lastTime = 0;
        for (MonitorPlugin p : usedMonitorPlugins){
            long last = p.getDataSource().getLastTimeForRecord(id);
            if(last>lastTime)
                lastTime=last;
        }
        if(lastTime>0) {
            Date date = new Date(lastTime);
            this.date_to = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);
        }
        else{
            this.date_to = "invalid";
        }
    }
}
