package de.msl.mobilesleeplab.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import de.msl.mobilesleeplab.R;
import de.msl.mobilesleeplab.model.User;
import de.msl.mobilesleeplab.utils.SleepManager;

/**
 * This activity allows to select and create users for the MSL
 */
public class UserActivity extends AppCompatActivity {

    private EditText editFirstName;
    private EditText editLastName;
    private Spinner spinnerSelectUser;

    private List<User> allUsers;
    private User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        editFirstName = (EditText) findViewById(R.id.editFirstName);
        editLastName = (EditText) findViewById(R.id.editLastName);
        spinnerSelectUser = (Spinner) findViewById(R.id.spinnerSelectUser);

        spinnerSelectUser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentUser = allUsers.get(position);
                Log.d("current User", currentUser.toString());
                SleepManager.getInstance().getUserManager().setCurrentUser(currentUser);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        fillSpinner();
    }

    public void clickRegisterUser(View view){
        User u = new User(editFirstName.getText().toString(), editLastName.getText().toString());
        SleepManager.getInstance().getUserManager().addNewUser(u);
        fillSpinner();
    }

    public void fillSpinner(){
        allUsers = SleepManager.getInstance().getUserManager().getAllUsers();
        List<String> allUsersString = new ArrayList<>();

        if(allUsers != null && allUsers.size()>0){
            for(User u : allUsers){
                allUsersString.add(u.toString());
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, allUsersString);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSelectUser.setAdapter(adapter);
    }
}
