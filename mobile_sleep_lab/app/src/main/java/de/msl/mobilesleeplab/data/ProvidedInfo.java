package de.msl.mobilesleeplab.data;

import java.util.ArrayList;

import de.msl.mobilesleeplab.model.ValueMeaning;

/**
 * Created by Andreas on 03.03.2016.
 * This class represents the data which certain plugins provide to the core application
 */
public class ProvidedInfo {

    private String TABLENAME;
    public String COLUMN_ID = "_id";
    public String COLUMN_RECORD = "record";
    public String COLUMN_TIME_FROM = "time_from";
    public String COLUMN_TIME_TO = "time_to";
    private ArrayList<String> COLUMNS;

    private String createCommand;

    private ArrayList<ValueMeaning> valueMeanings;

    /**
     *
     * @param tableName name of the table, where this plugin data is stored
     * @param columns namings of the database columns, received by the plugin
     * @param nrOfInts nr of received ints
     * @param nrOfFloats nr of received floats
     * @param nrOfStrings nr of received strings
     */
    public ProvidedInfo(String tableName, ArrayList<String> columns, int nrOfInts, int nrOfFloats, int nrOfStrings){
        this.TABLENAME = tableName;
        this.COLUMNS = columns;

        valueMeanings = new ArrayList<>();

        int i = 0;

        createCommand = "create table " + TABLENAME + "(" + COLUMN_ID + " integer primary key autoincrement, " + COLUMN_RECORD + " integer not null, " + COLUMN_TIME_FROM + " string not null, " + COLUMN_TIME_TO + " string not null, ";

        if(COLUMNS!= null){
            for(String COLUMN : COLUMNS){
                valueMeanings.add(new ValueMeaning(COLUMN));

                createCommand += COLUMN + " ";
                if(i < (nrOfInts)){
                    createCommand += "integer not null";
                }
                else{
                    if(i < (nrOfInts+nrOfFloats)){
                        createCommand += "float not null";
                    }
                    else{
                        createCommand += "text not null";
                    }
                }
                if(i < (nrOfInts + nrOfFloats + nrOfStrings-1)){
                    createCommand +=", ";
                }
                i++;
            }
        }
        createCommand +=");";
    }

    public String getTABLENAME() {
        return TABLENAME;
    }

    public ArrayList<String> getCOLUMNS() {
        return COLUMNS;
    }

    /**
     * Returns the sql command, used to create the database
     * @return create command
     */
    public String getCreateCommand(){
        return createCommand;
    }

    /**
     * Returns the provided values in the better processable ValueMeaning format
     * @return ProvidedInfo of the plugin as ArrayList of ValueMeanings
     */
    public ArrayList<ValueMeaning> getValueMeanings() {
        return valueMeanings;
    }

}
