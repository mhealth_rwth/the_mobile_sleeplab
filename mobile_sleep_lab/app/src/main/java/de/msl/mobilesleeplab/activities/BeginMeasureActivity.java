package de.msl.mobilesleeplab.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import de.msl.mobilesleeplab.R;
import de.msl.mobilesleeplab.utils.SleepManager;

/**
 * This Activity represents the start measurement interface of the MSL
 */
public class BeginMeasureActivity extends AppCompatActivity {

    TextView textView;
    TextView pluginList;
    TextView textViewCurrentUser;
    Button button;
    EditText editTextDescription;


    /**
     * Initializes all necessary UI elements
     * @param savedInstanceState savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_begin_measure);
        textView = (TextView) findViewById(R.id.textView);
        pluginList = (TextView) findViewById(R.id.pluginList);
        textViewCurrentUser = (TextView) findViewById(R.id.textViewUser);
        editTextDescription = (EditText) findViewById(R.id.editTextDescription);
        pluginList.setText(SleepManager.getInstance().getPluginManager().getActiveMonitorPluginNameList());
        textViewCurrentUser.setText(getUser());
        setScreen();
    }

    /**
     * Restores the context when user comes back to this activity and checks whethere measurement is running or not
     */
    @Override
    public void onResume(){
        super.onResume();
        pluginList.setText(SleepManager.getInstance().getPluginManager().getActiveMonitorPluginNameList());
        textViewCurrentUser.setText(getUser());
        setScreen();
    }

    /**
     * Starts a new measurement
     * @param view View
     */
    public void startNewMeasurement(View view){
        String description = editTextDescription.getText().toString();
        if(SleepManager.getInstance().getMeasurementManager().isOngoingMeasurement()){
            SleepManager.getInstance().getMeasurementManager().stopMeasurement(this);
            setScreen();
        }
        else{
            SleepManager.getInstance().getMeasurementManager().startNewMeasurement(this, description);
            setScreen();
        }
    }

    /**
     * Switches UI elements based on whether measurement is running or not
     */
    public void setScreen(){
        if(button == null){
            button = (Button) findViewById(R.id.btn_startNewMeasurement);
        }
        if(!SleepManager.getInstance().getMeasurementManager().isOngoingMeasurement()){
            button.setText("Start Measurement");
        }
        else{
            button.setText("Stop Measurement");
        }
    }

    /**
     * Returns the name of the current user
     * @return Name of current user
     */
    private String getUser(){
        String res = "Current User: ";
        String name = "";
        try{
            name = SleepManager.getInstance().getUserManager().getCurrentUser().toString();
        }
        catch (Exception e){
            name = SleepManager.getInstance().getUserManager().getAllUsers().get(0).toString();
        }
        return res + name;
    }
}
