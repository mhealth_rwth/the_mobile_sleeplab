package de.msl.mobilesleeplab.model;

/**
 * Created by Andreas on 03.03.2016.
 * This class reprents a user of the application
 */
public class User {

    private long id;
    private String firstname;
    private String lastname;

    /**
     *
     * @param firstname the user's firstname
     * @param lastname the user's lastname
     */
    public User(String firstname, String lastname){
        this.firstname = firstname;
        this.lastname = lastname;
    }

    /**
     *
     * @param id the user's database id
     * @param firstname the user's firstname
     * @param lastname the user's lastname
     */
    public User(long id, String firstname, String lastname){
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setId(long id){
        this.id = id;
    }

    public long getId(){
        return this.id;
    }

    @Override
    public String toString(){
        return firstname + " " + lastname + " (" + id + ")";
    }

}
