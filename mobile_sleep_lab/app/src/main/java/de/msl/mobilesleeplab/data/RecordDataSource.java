package de.msl.mobilesleeplab.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.msl.mobilesleeplab.model.Record;
import de.msl.mobilesleeplab.utils.SleepManager;

/**
 * Created by Andreas on 03.03.2016.
 * Datasource for a table which stores information about Records
 */
public class RecordDataSource {

    private SQLiteDatabase database;
    private CustomSQLHelper databaseHelper;

    private static final String COLUMN_DATE_FROM = "date_from";

    private static final String TABLENAME = "records";

    public RecordDataSource(){
        databaseHelper = SleepManager.getInstance().getPluginManager().getCustomSQLHelper();
    }

    public void open() throws SQLException {
        database = databaseHelper.getWritableDatabase();
    }

    public void close(){
        databaseHelper.close();
    }

    /**
     * Store a new record to the database
     * @param record Record to be stored
     */
    public void createRecord(Record record){
        try{
            open();
            String command = "insert into records values (";
            command += record.getId();
            command += ", '" + record.getDateFrom();
            command += "', '" + record.getDateTo();
            command += "', '" + record.getDescription();
            command += "', '" + record.pluginsToString();
            command += "', '" + record.getUser().getId();
            command += "');";
            database.execSQL(command);
            close();
        }
        catch(Exception e){

        }

    }

    /**
     * Update values of a stored record
     * @param record record to be updated
     */
    public void updateRecord(Record record){
        try{
            open();
            String command = "update records set date_to = '" + record.getDateTo() + "'," +
                    "date_from = '" + record.getDateFrom() + "'," +
                    "description = '" + record.getDescription() + "', "+
                    "plugins = '" + record.pluginsToString() + "' "+
                    "where _id=" + record.getId();
            database.execSQL(command);
            close();
        }
        catch (Exception e){

        }

    }

    /**
     * Get all Records which are stored in the database
     * @return List of all Records
     */
    public List<Record> getAllRecords(){
        List<Record> result = new ArrayList<>();
        String orderBy =  RecordDataSource.COLUMN_DATE_FROM + " DESC";
        database = SleepManager.getInstance().getPluginManager().getCustomSQLHelper().getWritableDatabase();
        Cursor cursor = database.query(TABLENAME, new String[]{"_id", "date_from", "date_to", "description", "plugins", "user"}, null, null, null, null, orderBy);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Record record = cursorToRecord(cursor);
            result.add(record);
            cursor.moveToNext();
        }
        return result;
    }


    /**
     * Get a record by its record id
     * @param id id of record
     * @return Record with respective id
     */
    public Record getRecordById(long id){
        database = SleepManager.getInstance().getPluginManager().getCustomSQLHelper().getWritableDatabase();
        String selection = "_id = " + id;
        Cursor cursor = database.query(TABLENAME, new String[]{"_id", "date_from", "date_to", "description", "plugins", "user"}, selection, null, null, null, null);
        cursor.moveToFirst();
        Record result = cursorToRecord(cursor);
        return result;
    }

    /**
     * Returns the newest record
     * @return newest record
     */
    public Record getLastRecord(){
        database = SleepManager.getInstance().getPluginManager().getCustomSQLHelper().getWritableDatabase();
        Cursor cursor = database.query(TABLENAME, new String[]{"_id", "date_from", "date_to", "description", "plugins", "user"}, null, null, null, null, null);
        cursor.moveToLast();
        Record result = cursorToRecord(cursor);
        return result;
    }


    /**
     * Translates the database cursor to a Record object
     * @param cursor cursor, returned by database
     * @return Record object
     */
    public Record cursorToRecord(Cursor cursor){
        Record record = new Record();
        record.setId(cursor.getLong(0));
        record.setDateFrom(cursor.getString(1));
        record.setDateTo(cursor.getString(2));
        record.setDescription(cursor.getString(3));
        record.setUsedMonitorPlugins(cursor.getString(4));
        record.setUser(SleepManager.getInstance().getUserManager().getUserById(cursor.getInt(5)));
        return record;
    }
}
