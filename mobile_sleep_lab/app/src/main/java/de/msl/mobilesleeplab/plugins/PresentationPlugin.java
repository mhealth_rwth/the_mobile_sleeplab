package de.msl.mobilesleeplab.plugins;

import android.content.pm.ResolveInfo;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Collections;

import de.msl.mobilesleeplab.data.NeededInfo;

/**
 * Created by Andreas on 28.04.2016.
 * Representation of Presentation Plugin
 */
public class PresentationPlugin extends Plugin{

    private NeededInfo neededInfo;

    /**
     * @param info Info Message which is provided by the plugin's manifest
     */
    public PresentationPlugin(ResolveInfo info) {

        super(info, false);

        Bundle bundle = info.serviceInfo.metaData;

        int neededNrOfInts = bundle.getInt("neededNrOfInts");
        int neededNrOfFloats = bundle.getInt("neededNrOfFloats");
        int neededNrOfStrings = bundle.getInt("neededNrOfStrings");
        String neededIntMeanings[] = bundle.getString("neededMeaningOfInts").split(";");
        String neededFloatMeanings[] = bundle.getString("neededMeaningOfFloats").split(";");
        String neededStringMeanings[] = bundle.getString("neededMeaningOfStrings").split(";");

        ArrayList<String> neededIntMeaning = new ArrayList<>();
        ArrayList<String> neededFloatMeaning = new ArrayList<>();
        ArrayList<String> neededStringMeaning = new ArrayList<>();

        if(neededNrOfInts>0)
            Collections.addAll(neededIntMeaning,neededIntMeanings );
        if(neededNrOfFloats>0)
            Collections.addAll(neededFloatMeaning,neededFloatMeanings );
        if(neededNrOfStrings>0)
            Collections.addAll(neededStringMeaning,neededStringMeanings );

        neededInfo = new NeededInfo(neededNrOfInts, neededNrOfFloats, neededNrOfStrings, neededIntMeaning, neededFloatMeaning, neededStringMeaning);

    }

    /**
     * Returns the needed infos for the presentation plugin
     * @return needed info of plugin
     */
    public NeededInfo getNeededInfo() {
        return neededInfo;
    }
}
