package de.msl.mobilesleeplab.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import de.msl.mobilesleeplab.R;
import de.msl.mobilesleeplab.utils.SleepManager;

/**
 * Starting Activity of the application. Shows menu for starting measurements or inspecting records
 * An additional context menu allows to modify settings
 */
public class MainMenuActivity extends AppCompatActivity {

    // Main Menu Buttons

    private Button btnNewMeasurment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        initialisation();



        btnNewMeasurment = (Button) findViewById(R.id.btn_new_measurement);
        Button btnRecords = (Button) findViewById(R.id.btn_records);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(SleepManager.getInstance().getMeasurementManager().isOngoingMeasurement()){
            btnNewMeasurment.setText("Ongoing Measurement");
        }
        else{
            btnNewMeasurment.setText("Start New Measurement");
        }
    }

    public void startMeasurement(View view){
        Intent intent = new Intent(this, BeginMeasureActivity.class);
        startActivity(intent);
    }

    public void showRecords(View view){
        Intent intent = new Intent(this, ListRecordsActivity.class);
        startActivity(intent);
    }

    public void initialisation(){

        SleepManager.getInstance().init(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case R.id.generalsettings:
                Intent intent = new Intent(this, PluginSettings.class);
                startActivity(intent);
                break;
            case R.id.usersettings:
                Intent uintent = new Intent(this, UserActivity.class);
                startActivity(uintent);
                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
