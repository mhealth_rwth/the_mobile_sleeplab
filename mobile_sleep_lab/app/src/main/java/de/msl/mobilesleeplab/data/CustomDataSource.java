package de.msl.mobilesleeplab.data;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.msl.mobilesleeplab.model.Observation;
import de.msl.mobilesleeplab.utils.SleepManager;

/**
 * Created by Andreas on 11.02.16.
 * This class describes a dynamic DataSource for Plugins, which is created based on the plugin's needs.
 */
public class CustomDataSource {

    private SQLiteDatabase database;

    public void setDbHelper(CustomSQLHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    private CustomSQLHelper dbHelper;

    private String[] allColumnsButId;

    private int nrOfInts;
    private int nrOfFloats;
    private int nrOfStrings;

    private ArrayList<String> COLUMNS;

    private String tablename;

    /**
     * Constructor for a CustomDataSource
     * @param nrOfInts nr of integers the plugin provides
     * @param nrOfFloats nr of floats the plugin provides
     * @param nrOfStrings nr of strings the plugin provides
     * @param tablename tablename for this plugin
     * @param COLUMNS column names of the new table
     */
    public CustomDataSource(int nrOfInts, int nrOfFloats, int nrOfStrings, String tablename, ArrayList<String> COLUMNS){
        this.nrOfInts = nrOfInts;
        this.nrOfFloats = nrOfFloats;
        this.nrOfStrings = nrOfStrings;
        this.dbHelper = SleepManager.getInstance().getPluginManager().getCustomSQLHelper();
        allColumnsButId = COLUMNS.toArray(new String[COLUMNS.size()]);
        this.tablename = tablename;
        this.COLUMNS = COLUMNS;
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    /**
     * Adds a new set of values to a Plugin databse
     * @param time_from start time of the observation
     * @param time_to end time of the observation
     * @param intValues integer values of the observation
     * @param floatValues float values of the observation
     * @param stringValues string values of the observation
     * @param context application context
     * @param name name of the sending plugin
     */
    public void createValue(String time_from, String time_to, ArrayList<Integer> intValues, ArrayList<Float> floatValues, ArrayList<String> stringValues, Context context, String name){
        ContentValues sendValues = new ContentValues();

        String columns = "time_from, time_to, record, ";
        String values = "'" + time_from + "', " + "'" + time_to + "', " + SleepManager.getInstance().getRecordManager().getCurrentRecord().getId() + ", ";

        ContentResolver cr = context.getContentResolver();

        ContentValues contentValues = new ContentValues();

        contentValues.put("TIME_FROM", time_from);
        contentValues.put("TIME_TO", time_to);
        contentValues.put("RECORD", SleepManager.getInstance().getRecordManager().getCurrentRecord().getId());



        int i = 0;
        if(intValues!=null){
            if(intValues.size()>0){
                for(int value : intValues){
                    sendValues.put(COLUMNS.get(i), value);
                    columns += COLUMNS.get(i);
                    columns += ", ";
                    values += value;
                    values += ", ";
                    contentValues.put(COLUMNS.get(i), value);
                    i++;
                }
            }
        }

        if(floatValues!=null){
            if(floatValues.size()>0){
                for(float value : floatValues){
                    sendValues.put(COLUMNS.get(i), value);
                    columns += COLUMNS.get(i);
                    columns += ", ";
                    values += value;
                    values += ", ";
                    contentValues.put(COLUMNS.get(i) , value);
                    i++;
                }
            }
        }

        if(stringValues!=null){
            if(stringValues.size()>0){
                for(String value : stringValues){
                    sendValues.put(COLUMNS.get(i), value);
                    columns += COLUMNS.get(i);
                    columns += ", ";
                    values += "\'" + value + "\'";
                    values += ", ";
                    contentValues.put(COLUMNS.get(i), value);
                    i++;
                }
            }
        }

        cr.insert(Uri.parse("content://" + MslContentProvider.AUTHORITY + "/" + name), contentValues );

    }

    /**
     * Returns all stored values from the table.
     * @return All values wrapped to the Observation class
     */
    public List<Observation> getAllValues(){
        List<Observation> observations = new ArrayList<>();
        Cursor cursor = database.query(tablename, allColumnsButId, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Observation observation = cursorToObservation(cursor);
            observations.add(observation);
            cursor.moveToNext();
        }
        cursor.close();
        return observations;
    }

    /**
     * Returns all values which were stored with a certain record id
     * @param recordId record id of needed values
     * @return Values wrapped in Observation class
     */
    public List<Observation> getAllValuesForRecord(long recordId){
        List<Observation> observations = new ArrayList<>();
        String selection = "record = " + recordId;
        database = SleepManager.getInstance().getPluginManager().getCustomSQLHelper().getWritableDatabase();
        Cursor cursor = database.query(tablename, allColumnsButId, selection, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Observation observation = cursorToObservation(cursor);
            observations.add(observation);
            cursor.moveToNext();
        }
        cursor.close();
        return observations;
    }

    /**
     * Returns the latest time stamp of a certain record
     * @param recordId record id of needed time stamp
     * @return timestamp in ms
     */
    public long getLastTimeForRecord(long recordId){
        long lastEntry = 0;
        String selection = "record = " + recordId;
        database = SleepManager.getInstance().getPluginManager().getCustomSQLHelper().getWritableDatabase();
        Cursor cursor = database.query(tablename, allColumnsButId, selection, null, null, null, null);
        try {
            cursor.moveToLast();
            lastEntry = cursor.getLong(1);
        }
        catch (Exception ex){
            lastEntry = 0;
        }
        cursor.close();
        return lastEntry;
    }

    /**
     * Translates a Cursor element to an Observation
     * @param cursor cursor element from database
     * @return new Observation object
     */
    public Observation cursorToObservation(Cursor cursor){
        ArrayList<Integer> ints = new ArrayList<>();
        ArrayList<Float> floats = new ArrayList<>();
        ArrayList<String> strings = new ArrayList<>();

        int i = 0;
        while(i < (nrOfInts+nrOfFloats+nrOfStrings)){
            if(i<nrOfInts){
                ints.add(cursor.getInt(i));
                i++;
            }
            else{
                if(i < (nrOfInts + nrOfFloats)){
                    floats.add(cursor.getFloat(i));
                    i++;
                }
                else{
                    strings.add(cursor.getString(i));
                    i++;
                }
            }
        }
        return new Observation(ints, floats, strings);

    }
}
